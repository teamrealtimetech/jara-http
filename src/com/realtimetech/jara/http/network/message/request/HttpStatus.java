package com.realtimetech.jara.http.network.message.request;

public enum HttpStatus {
	WAIT, READY
}
