package com.realtimetech.jara.http.network.message.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.header.ContentTypeHeader;
import com.realtimetech.jara.http.header.MethodHeader;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.header.type.MethodType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.Content;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.util.HttpStreamUtil;

public class HttpRequest {
	private Stream stream;

	private String path;

	private boolean endHeaders;
	private ByteArrayOutputStream headerByteStream;

	private boolean endStream;
	private ByteArrayOutputStream dataByteStream;

	private HashMap<String, String> parameters;

	private Content content;
	private HeaderSet headerSet;

	private byte[] data;

	public HttpRequest(Stream stream) {
		this.stream = stream;

		this.path = "";

		this.parameters = new HashMap<String, String>();

		this.endHeaders = false;
		this.headerByteStream = null;

		this.endStream = false;
		this.dataByteStream = null;

		this.content = null;
		this.headerSet = null;
		this.data = null;
	}

	public void build() {
		parameters.clear();

		// Parse PATH step.
		{
			TypeHeader header = getHeaderSet().get(HeaderType.PATH);

			if (header != null) {
				this.path = header.getValue();

				if (path.contains("?")) {
					String[] pathSplit = path.split("\\?");
					this.path = pathSplit[0];
					parameters.putAll(HttpStreamUtil.readParameters(pathSplit[1], HttpServer.DEFAULT_CHARSET));
				}
			}
		}

		// Parse CONTENT step.
		{
			TypeHeader header = getHeaderSet().get(HeaderType.CONTENT_TYPE);

			if (header != null) {
				ContentTypeHeader contentTypeHeader = (ContentTypeHeader) header;
				MimeType mimeType = contentTypeHeader.getMimeType();

				this.content = mimeType.buildContent(headerSet, data);
			}
		}
	}

	public Stream getStream() {
		return stream;
	}

	public void appendHeaderBytes(byte[] bytes, boolean endHeaders, boolean endStream) throws IOException, ProtocolErrorException {
		if (this.endHeaders)
			return;

		if (this.headerByteStream == null) {
			this.headerByteStream = new ByteArrayOutputStream();
		}

		this.headerByteStream.write(bytes);

		if (endHeaders) {
			if (endStream) {
				this.endStream = true;
			}
			this.endHeaders = true;
			this.headerSet = stream.getConnection().getReceiveHpack().decode(this.headerByteStream.toByteArray());
			this.headerByteStream = null;
		}
	}

	public void appendDataBytes(byte[] bytes, boolean endStream) throws IOException {
		if (this.endStream)
			return;

		if (this.dataByteStream == null) {
			this.dataByteStream = new ByteArrayOutputStream();
		}

		this.dataByteStream.write(bytes);

		if (endStream) {
			this.endStream = true;
			this.data = this.dataByteStream.toByteArray();

			this.dataByteStream = null;
		}
	}

	public boolean isHasHeaders() {
		return !(!endHeaders && this.headerByteStream == null);
	}

	public boolean isEndHeaders() {
		return endHeaders;
	}

	public boolean isEndStream() {
		return endStream;
	}

	public Content getContent() {
		return content;
	}

	public HeaderSet getHeaderSet() {
		return headerSet;
	}

	public HashMap<String, String> getParameters() {
		return parameters;
	}

	public String getPath() {
		return path;
	}

	public MimeType getMimeType() {
		TypeHeader typeHeader = headerSet.get(HeaderType.CONTENT_TYPE);

		if (typeHeader instanceof ContentTypeHeader) {
			ContentTypeHeader contentTypeHeader = (ContentTypeHeader) typeHeader;

			return contentTypeHeader.getMimeType();
		}
		return MimeType.ALL;
	}

	public MethodType getMethodType() {
		TypeHeader typeHeader = headerSet.get(HeaderType.METHOD);

		if (typeHeader instanceof MethodHeader) {
			MethodHeader methodHeader = (MethodHeader) typeHeader;
			
			return methodHeader.getMethodType();
		}
		
		return MethodType.ALL;
	}
}
