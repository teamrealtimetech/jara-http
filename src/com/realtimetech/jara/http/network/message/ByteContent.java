package com.realtimetech.jara.http.network.message;

import java.nio.charset.Charset;

import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.header.ContentTypeHeader;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.header.type.MimeType;

public class ByteContent extends Content {
	private byte[] bytes;

	private Charset charset;
	private MimeType mimeType;

	@Override
	public void clear() {
		this.bytes = null;

		this.charset = HttpServer.DEFAULT_CHARSET;
		this.mimeType = MimeType.ALL;
	}

	@Override
	public void read(HeaderSet headerSet, byte[] bytes) {
		TypeHeader typeContentHeader = headerSet.get(HeaderType.CONTENT_TYPE);

		if (typeContentHeader != null && typeContentHeader instanceof ContentTypeHeader) {
			ContentTypeHeader contentTypeHeader = (ContentTypeHeader) typeContentHeader;

			this.mimeType = contentTypeHeader.getMimeType();
			this.charset = contentTypeHeader.getCharset();
		}

		this.bytes = bytes;
	}

	@Override
	public byte[] write(HeaderSet headerSet) {
		ContentTypeHeader contentTypeHeader = new ContentTypeHeader(mimeType);
		contentTypeHeader.setMimeType(mimeType);
		contentTypeHeader.setCharset(charset);
		contentTypeHeader.setBoundaryKey(null);

		headerSet.set(contentTypeHeader);

		return bytes;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public MimeType getMimeType() {
		return mimeType;
	}

	public void setMimeType(MimeType mimeType) {
		this.mimeType = mimeType;
	}

	public String getString() {
		return new String(this.bytes, charset);
	}

	public void setString(String string) {
		this.mimeType = MimeType.TEXT_PLAIN;
		this.bytes = string.getBytes(charset);
	}
}
