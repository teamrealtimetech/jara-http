package com.realtimetech.jara.http.network.message;

import com.realtimetech.jara.http.header.collection.HeaderSet;

public abstract class Content {
	public Content() {
		this.clear();
	}
	
	public abstract void clear();
	
	public abstract void read(HeaderSet headerSet, byte[] bytes);

	public abstract byte[] write(HeaderSet headerSet);
}
