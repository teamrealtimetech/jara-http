package com.realtimetech.jara.http.network.message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;

import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.header.ContentDispositionHeader;
import com.realtimetech.jara.http.header.ContentTypeHeader;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.util.HttpStreamUtil;

public class MultipartContent extends Content {
	private String boundaryKey;
	private Charset charset;

	private HashMap<String, PartContent> partContents;

	@Override
	public void clear() {
		this.boundaryKey = null;
		this.charset = HttpServer.DEFAULT_CHARSET;
		
		this.partContents = new HashMap<String, PartContent>();
	}

	@Override
	public void read(HeaderSet headerSet, byte[] bytes) {
		partContents.clear();

		TypeHeader typeContentHeader = headerSet.get(HeaderType.CONTENT_TYPE);

		if (typeContentHeader != null && typeContentHeader instanceof ContentTypeHeader) {
			ContentTypeHeader contentTypeHeader = (ContentTypeHeader) typeContentHeader;

			this.charset = contentTypeHeader.getCharset();
			this.boundaryKey = contentTypeHeader.getBoundaryKey();
		}

		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

		final byte[] STARTING_BYTES = ("--" + this.boundaryKey + "\r\n").getBytes();
		final byte[] PART_BYTES = ("\r\n--" + this.boundaryKey).getBytes();

		try {
			HttpStreamUtil.readByteAtBytes(byteArrayInputStream, STARTING_BYTES);
			while (byteArrayInputStream.available() > 4) {
				List<String> headerString = HttpStreamUtil.readHeaderStringList(byteArrayInputStream, charset);

				byte[] partBytes = HttpStreamUtil.readByteAtBytes(byteArrayInputStream, PART_BYTES);
				
				HeaderSet partHeaderSet = HttpStreamUtil.readHeaders(headerString);

				ContentDispositionHeader contentDispositionHeader = (ContentDispositionHeader) partHeaderSet.get(HeaderType.CONTENT_DISPOSITION);

				PartContent partContent;
				if (contentDispositionHeader != null) {
					if(contentDispositionHeader.getFileName() != null) {
						partContent = new PartFileContent();
					}else {
						partContent = new PartNormalContent();
					}
					
					partContent.read(partHeaderSet, partBytes);
					partContents.put(contentDispositionHeader.getContentName(), partContent);
				}
			}
		} catch (IOException e) {
		}
	}

	@Override
	public byte[] write(HeaderSet headerSet) {
		ContentTypeHeader contentTypeHeader = new ContentTypeHeader(MimeType.MULTIPART_FORM_DATA);
		contentTypeHeader.setCharset(charset);
		contentTypeHeader.setBoundaryKey(boundaryKey);

		headerSet.set(contentTypeHeader);
		
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		final byte[] STARTING_BYTES = ("--" + this.boundaryKey + "\r\n").getBytes();
		final byte[] PART_BYTES = ("\r\n--" + this.boundaryKey).getBytes();
		final byte[] END_BYTES = ("--\r\n").getBytes();

		try {
			arrayOutputStream.write(STARTING_BYTES);

			for (String name : partContents.keySet()) {
				PartContent postContent = partContents.get(name);

				HeaderSet partHeaderSet = new HeaderSet();

				byte[] bytes = postContent.write(partHeaderSet);

				HttpStreamUtil.writeHeaderStringList(arrayOutputStream, partHeaderSet.toList(), charset);

				arrayOutputStream.write(bytes);

				arrayOutputStream.write(PART_BYTES);
			}
			arrayOutputStream.write(END_BYTES);
			arrayOutputStream.flush();
		} catch (IOException e) {
		}
		
		return arrayOutputStream.toByteArray();
	}

	public String getBoundaryKey() {
		return boundaryKey;
	}

	public void setBoundaryKey(String boundaryKey) {
		this.boundaryKey = boundaryKey;
	}

	public HashMap<String, PartContent> getPartContents() {
		return partContents;
	}
}
