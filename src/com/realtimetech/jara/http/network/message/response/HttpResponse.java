package com.realtimetech.jara.http.network.message.response;

import java.io.IOException;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.network.message.Content;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.request.HttpStatus;
import com.realtimetech.jara.http.network.stream.Stream;

public class HttpResponse {
	private Stream stream;

	private HttpRequest httpRequest;
	
	private HttpStatus httpStatus;

	private HeaderSet headerSet;
	private Content content;

	private int responseCode;

	public HttpResponse(HttpRequest httpRequest, Stream stream) {
		this.stream = stream;
		this.httpStatus = HttpStatus.READY;
		this.httpRequest = httpRequest;
		this.headerSet = new HeaderSet();
		this.responseCode = 404;
	}
	
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public HttpRequest getHttpRequest() {
		return httpRequest;
	}

	public Stream getStream() {
		return stream;
	}

	public HeaderSet getHeaderSet() {
		return headerSet;
	}

	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	
	public void response() {
		byte[] build = build();

		try {
			stream.sendHeaderSet(getHeaderSet(), build == null);
			if (build != null) {
				stream.sendData(build);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ProtocolErrorException e) {
			e.printStackTrace();
			stream.getConnection().sendProtocolError(e);
		}
	}
	
	public void pending() {
		this.httpStatus = HttpStatus.WAIT;
	}
	

	public byte[] build() {
		byte[] write = null;
		if (content != null) {
			write = content.write(headerSet);
		}

		headerSet.set(HeaderType.STATUS, responseCode + "");
		return write;
	}
}