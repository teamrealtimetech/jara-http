package com.realtimetech.jara.http.network.message;

import java.nio.charset.Charset;
import java.util.HashMap;
import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.header.ContentTypeHeader;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.util.HttpStreamUtil;

public class FormContent extends Content {
	private Charset charset;

	private HashMap<String, String> contents;

	@Override
	public void clear() {
		this.charset = HttpServer.DEFAULT_CHARSET;
		this.contents = new HashMap<String, String>();
	}

	@Override
	public void read(HeaderSet headerSet, byte[] bytes) {
		contents.clear();

		TypeHeader typeContentHeader = headerSet.get(HeaderType.CONTENT_TYPE);

		if (typeContentHeader != null && typeContentHeader instanceof ContentTypeHeader) {
			ContentTypeHeader contentTypeHeader = (ContentTypeHeader) typeContentHeader;

			this.charset = contentTypeHeader.getCharset();
		}

		HashMap<String, String> parametersInString = HttpStreamUtil.readParameters(new String(bytes, this.charset), this.charset);

		for (String parameterKey : parametersInString.keySet()) {
			contents.put(parameterKey, parametersInString.get(parameterKey));
		}
	}

	@Override
	public byte[] write(HeaderSet headerSet) {
		ContentTypeHeader contentTypeHeader = new ContentTypeHeader(MimeType.APPLICATION_X_WWW_FORM_URLENCODED);
		contentTypeHeader.setCharset(charset);
		contentTypeHeader.setBoundaryKey(null);

		headerSet.set(contentTypeHeader);

		String wrtieParameters = HttpStreamUtil.wrtieParameters(contents, this.charset);

		return wrtieParameters.getBytes(charset);
	}

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public HashMap<String, String> getContents() {
		return contents;
	}
}
