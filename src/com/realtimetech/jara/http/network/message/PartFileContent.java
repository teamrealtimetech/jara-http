package com.realtimetech.jara.http.network.message;

import com.realtimetech.jara.http.header.ContentDispositionHeader;
import com.realtimetech.jara.http.header.ContentTypeHeader;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.type.DispositionType;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.header.type.MimeType;

public class PartFileContent extends PartContent {
	private byte[] bytes;

	private DispositionType dispositionType;
	private String name;
	private String fileName;

	private MimeType mimeType;

	@Override
	public void clear() {
		this.bytes = null;

		this.dispositionType = DispositionType.FORMDATA;
		this.name = "";
		this.fileName = "";

		this.mimeType = MimeType.ALL;
	}

	@Override
	public void read(HeaderSet headerSet, byte[] bytes) {
		TypeHeader typeContentDispositionHeader = headerSet.get(HeaderType.CONTENT_DISPOSITION);

		if (typeContentDispositionHeader != null && typeContentDispositionHeader instanceof ContentDispositionHeader) {
			ContentDispositionHeader contentDispositionHeader = (ContentDispositionHeader) typeContentDispositionHeader;

			this.dispositionType = contentDispositionHeader.getDispositionType();
			this.name = contentDispositionHeader.getContentName();
			this.fileName = contentDispositionHeader.getFileName();
		}

		TypeHeader typeContentHeader = headerSet.get(HeaderType.CONTENT_TYPE);

		if (typeContentHeader != null && typeContentHeader instanceof ContentTypeHeader) {
			ContentTypeHeader contentTypeHeader = (ContentTypeHeader) typeContentHeader;

			this.mimeType = contentTypeHeader.getMimeType();
		}

		this.bytes = bytes;
	}

	@Override
	public byte[] write(HeaderSet headerSet) {
		ContentDispositionHeader contentDispositionHeader = new ContentDispositionHeader(dispositionType, name, fileName);

		headerSet.set(contentDispositionHeader);

		ContentTypeHeader contentTypeHeader = new ContentTypeHeader(mimeType);
		contentTypeHeader.setMimeType(mimeType);
		contentTypeHeader.setCharset(null);
		contentTypeHeader.setBoundaryKey(null);

		headerSet.set(contentTypeHeader);

		return bytes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public MimeType getMimeType() {
		return mimeType;
	}

	public void setMimeType(MimeType mimeType) {
		this.mimeType = mimeType;
	}
}
