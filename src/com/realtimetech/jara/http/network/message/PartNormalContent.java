package com.realtimetech.jara.http.network.message;

import java.nio.charset.Charset;

import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.header.ContentDispositionHeader;
import com.realtimetech.jara.http.header.ContentTypeHeader;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.type.DispositionType;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.header.type.MimeType;

public class PartNormalContent extends PartContent {
	private byte[] bytes;

	private DispositionType dispositionType;
	private String name;
	
	private Charset charset;
	private MimeType mimeType;

	@Override
	public void clear() {
		this.bytes = null;
		
		this.dispositionType = DispositionType.FORMDATA;
		this.name = "";

		this.charset = HttpServer.DEFAULT_CHARSET;
		this.mimeType = MimeType.ALL;
	}

	@Override
	public void read(HeaderSet headerSet, byte[] bytes) {
		TypeHeader typeContentDispositionHeader = headerSet.get(HeaderType.CONTENT_DISPOSITION);

		if (typeContentDispositionHeader != null && typeContentDispositionHeader instanceof ContentDispositionHeader) {
			ContentDispositionHeader contentDispositionHeader = (ContentDispositionHeader) typeContentDispositionHeader;

			this.dispositionType = contentDispositionHeader.getDispositionType();
			this.name = contentDispositionHeader.getName();
		}

		TypeHeader typeContentHeader = headerSet.get(HeaderType.CONTENT_TYPE);

		if (typeContentHeader != null && typeContentHeader instanceof ContentTypeHeader) {
			ContentTypeHeader contentTypeHeader = (ContentTypeHeader) typeContentHeader;

			this.mimeType = contentTypeHeader.getMimeType();
			this.charset = contentTypeHeader.getCharset();
		}

		this.bytes = bytes;
	}

	@Override
	public byte[] write(HeaderSet headerSet) {
		ContentDispositionHeader contentDispositionHeader = new ContentDispositionHeader(dispositionType, name, null);

		headerSet.set(contentDispositionHeader);

		ContentTypeHeader contentTypeHeader = new ContentTypeHeader(mimeType);
		contentTypeHeader.setMimeType(mimeType);
		contentTypeHeader.setCharset(charset);

		headerSet.set(contentTypeHeader);

		return bytes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Charset getCharset() {
		return charset;
	}
	
	public DispositionType getDispositionType() {
		return dispositionType;
	}
	
	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public MimeType getMimeType() {
		return mimeType;
	}

	public void setMimeType(MimeType mimeType) {
		this.mimeType = mimeType;
	}
}
