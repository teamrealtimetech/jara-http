package com.realtimetech.jara.http.network.stream;

import java.util.concurrent.LinkedBlockingQueue;

import com.realtimetech.jara.http.connection.Connection;

/**
 * Stream 객체에 대한 Multi-Threading Processing을 위한 Worker Class이다.
 * 
 * @author ParkJeongHwan
 */
public class StreamThread implements Runnable {
	/**
	 * 실제 Thread 필드이며 이 Class는 Runnable Interface를 구현하므로 이 Thread가 Class의 run함수를 수행한다.
	 */
	private Thread thread;

	/**
	 * Worker에 해당되는 HTTP2 Connection 필드이다.
	 */
	private Connection connection;

	/**
	 * Worker Class에 대한 생성자 이다.
	 * 
	 * @param connection Worker에 해당하는 Connection 인자이다.
	 */
	public StreamThread(Connection connection) {
		this.connection = connection;

		this.thread = new Thread(this);
	}

	public Connection getConnection() {
		return connection;
	}

	/**
	 * Worker를 활성화 하는 메서드이다.
	 */
	public void active() {
		thread.start();
	}

	/**
	 * Worker를 비활성화 하는 메서드이다.
	 */
	public void deactive() {
		this.thread.interrupt();
	}

	/**
	 * 실제 Stream를 Processing하는 로직이 있는 메서드이다.
	 * 
	 * Connection의 처리가 필요한 Stream가 존재하는 Queue에서 Stream을 꺼내서 처리한다.  단, 해당 Stream의 처리를 다른 Worker에서 사용하고 있을경우 다시 Queue에 넣는다.
	 * 
	 * 이 Worker는 더 이상 처리할 Stream이 없다면 Rest Counting을 시작하며 일정 수치 이상의 Counting이 발생한다면 Worker를 중단합니다.
	 */
	@Override
	public void run() {
		LinkedBlockingQueue<Stream> streamQueue = connection.getStreamQueue();

		int restCount = 0;

		while (true) {
			Stream stream;
			try {
				if (streamQueue.size() != 0) {
					restCount = 0;
					stream = streamQueue.take();

					if (!stream.isRunning()) {
						stream.run();
					} else {
						streamQueue.offer(stream);
					}
				} else {
					restCount++;
					Thread.sleep(10);
				}

				if (restCount >= 300) {
					connection.removeStreamThread(this);
				}

			} catch (InterruptedException e) {
				break;
			}
		}
	}
}
