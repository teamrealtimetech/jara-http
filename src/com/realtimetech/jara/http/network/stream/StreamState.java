package com.realtimetech.jara.http.network.stream;

/**
 * Stream의 상태 Type에 대한 열거형이다.
 * 
 * @author ParkJeongHwan
 */
public enum StreamState {
	IDLE(0x0), RESERVED_LOCAL(0x1), RESERVED_REMOTE(0x2), OPEN(0x3), HALF_CLOSED_LOCAL(0x4), HALF_CLOSED_REMOTE(0x5), CLOSED(0x6);

	private int type;

	StreamState(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}
}
