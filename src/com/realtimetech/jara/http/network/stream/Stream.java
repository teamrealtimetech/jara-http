package com.realtimetech.jara.http.network.stream;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.realtimetech.jara.http.connection.Connection;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameOrder;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.ContinuationFrame;
import com.realtimetech.jara.http.network.frame.impl.DataFrame;
import com.realtimetech.jara.http.network.frame.impl.HeaderFrame;
import com.realtimetech.jara.http.network.frame.impl.structures.ContinuationStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.DataStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.GoAwayStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.HeaderStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.PriorityStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.PushPromiseStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.RstStreamStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.SettingStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.WindowUpdateStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.setting.SettingType;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.request.HttpStatus;
import com.realtimetech.jara.http.network.message.response.HttpResponse;
import com.realtimetech.jara.http.network.structure.Structure;

/**
 * HTTP2 프로토콜의 Stream 구현체이다.
 * 
 * @author ParkJeongHwan
 */
public class Stream implements Runnable {
	private boolean running = false;
	private Thread currentThread = null;

	/**
	 * CRLF에 대한 문자열을 미리 가지고 있는다.
	 */
	public static final String CRLF = "\r\n";

	/**
	 * Stream의 상태를 표현하는 StreamState이다.
	 */
	private StreamState state;

	/**
	 * Stream의 부모인 Connection이다.
	 */
	private Connection connection;

	/**
	 * Stream의 고유 id이다, unsigned 31-bit integer이다.
	 */
	private long id;

	private HashMap<FrameType, Structure> frameTypeByStrcuture;

	private ConcurrentLinkedQueue<FrameOrder> frameOrders;

	private boolean rstStatus;

	private HttpRequest httpRequest;

	/**
	 * Stream의 생성자이다.
	 * 
	 * @param connection 부모가 되는 Connection 인자이다.
	 * @param id         Connection으로부터 생성된 고유한 Stream Id 인자이다.
	 */
	public Stream(Connection connection, long id) {
		this.state = StreamState.IDLE;
		this.connection = connection;
		this.id = id;
		this.frameOrders = new ConcurrentLinkedQueue<FrameOrder>();
		this.rstStatus = false;

		this.frameTypeByStrcuture = new HashMap<FrameType, Structure>();
		{
			this.frameTypeByStrcuture.put(FrameType.DATA_FRAME, new DataStructure());
			this.frameTypeByStrcuture.put(FrameType.HEADER_FRAME, new HeaderStructure());
			this.frameTypeByStrcuture.put(FrameType.PRIORITY_FRAME, new PriorityStructure());
			this.frameTypeByStrcuture.put(FrameType.PUSH_PROMISE_FRAME, new PushPromiseStructure());
			this.frameTypeByStrcuture.put(FrameType.RST_STREAM_FRAME, new RstStreamStructure());
			this.frameTypeByStrcuture.put(FrameType.SETTING_FRAME, new SettingStructure());
			this.frameTypeByStrcuture.put(FrameType.WINDOW_UPDATE_FRAME, new WindowUpdateStructure());
			this.frameTypeByStrcuture.put(FrameType.GOAWAY_FRAME, new GoAwayStructure());
			this.frameTypeByStrcuture.put(FrameType.CONTINUATION, new ContinuationStructure());
		}

		this.httpRequest = new HttpRequest(this);
	}

	public HttpRequest getHttpRequest() {
		return httpRequest;
	}

	public boolean isRstStatus() {
		return rstStatus;
	}

	public void setRstStatus(boolean rstStatus) {
		this.rstStatus = rstStatus;
	}

	public ConcurrentLinkedQueue<FrameOrder> getFrameOrders() {
		return frameOrders;
	}

	public Structure getCacheStructure(FrameType frameType) {
		return this.frameTypeByStrcuture.get(frameType);
	}

	public StreamState getState() {
		return state;
	}

	public void setState(StreamState state) {
		this.state = state;
	}

	public Connection getConnection() {
		return connection;
	}

	public long getId() {
		return id;
	}

	@Override
	public synchronized void run() {
		this.running = true;
		this.currentThread = Thread.currentThread();
		try {
			FrameOrder frameOrder = frameOrders.poll();

			if (frameOrder != null) {
				frameOrder.process();

				if (state == StreamState.HALF_CLOSED_REMOTE) {
					httpRequest.build();

					HttpResponse httpResponse = new HttpResponse(httpRequest, this);
					getConnection().getHttpServer().processRequest(httpRequest, httpResponse);
					
					if (httpResponse.getHttpStatus() == HttpStatus.READY) {
						httpResponse.response();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ProtocolErrorException e) {
			e.printStackTrace();
			connection.sendProtocolError(e);
		}
		this.currentThread = null;
		this.running = false;
	}

	public Thread getCurrentThread() {
		return currentThread;
	}

	public boolean isRunning() {
		return running;
	}

	public void sendHeaderSet(HeaderSet headerSet, boolean endStream) throws IOException, ProtocolErrorException {
		byte[] encode = this.getConnection().getSendHpack().encode(headerSet);

		HeaderFrame headerFrame = new HeaderFrame(this);
		headerFrame.setEncodedData(encode);

		int maxFrameSize = getConnection().getRemoteSettings().get(SettingType.SETTINGS_MAX_FRAME_SIZE);

		int paddingSize = maxFrameSize / 10;
		int headerSplitSize = maxFrameSize - headerFrame.getPayloadStructure().getPartLength() - paddingSize;

		if (headerFrame.getPayloadStructure().getPartLength() <= maxFrameSize) {
			headerFrame.setEndHeaders(true);
			headerFrame.setEndStream(endStream);
		} else {
			headerFrame.setEncodedData(Arrays.copyOfRange(encode, 0, headerSplitSize - 1));
		}

		this.connection.sendFrame(headerFrame);

		if (!headerFrame.isEndHeaders()) {
			for (int index = headerSplitSize; index < encode.length; index += headerSplitSize) {
				ContinuationFrame continuationFrame = new ContinuationFrame(this);
				int to = index + headerSplitSize - 1;

				if (to >= encode.length) {
					to = encode.length;
				}

				continuationFrame.setEncodedData(Arrays.copyOfRange(encode, index, to));

				this.connection.sendFrame(continuationFrame);
			}

			HeaderFrame endHeaderFrame = new HeaderFrame(this);
			endHeaderFrame.setEncodedData(new byte[0]);
			endHeaderFrame.setEndHeaders(true);
			endHeaderFrame.setEndStream(endStream);

			this.connection.sendFrame(endHeaderFrame);
		}
	}

	public void sendData(byte[] bytes) throws IOException, ProtocolErrorException {
		int maxFrameSize = getConnection().getRemoteSettings().get(SettingType.SETTINGS_MAX_FRAME_SIZE) - 1;

		for (int index = 0; index < bytes.length; index += maxFrameSize) {
			DataFrame dataFrame = new DataFrame(this);
			int to = index + maxFrameSize;

			if (to >= bytes.length) {
				to = bytes.length;
				dataFrame.setEndStream(true);
			} else {
				dataFrame.setEndStream(false);
			}

			dataFrame.setData(Arrays.copyOfRange(bytes, index, to));

			this.connection.sendFrame(dataFrame);
		}
	}

	public void sendFrame(Frame<?> frame) throws IOException, ProtocolErrorException {
		this.connection.sendFrame(frame);
	}
}
