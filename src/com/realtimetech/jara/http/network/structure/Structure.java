package com.realtimetech.jara.http.network.structure;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;

public abstract class Structure {
	private static HashMap<Class<? extends Structure>, List<Field>> COMPILED_PART_STRUCTURE = new HashMap<Class<? extends Structure>, List<Field>>();
	private static HashMap<Class<? extends Structure>, List<Field>> COMPILED_FLAG_STRUCTURE = new HashMap<Class<? extends Structure>, List<Field>>();

	private List<Part<?>> parts;
	private List<Flag> flags;

	public Structure() {
		this.parts = null;
		this.flags = null;
	}

	public void mappingPart() {
		if (this.parts == null || this.flags == null) {
			this.parts = new ArrayList<Part<?>>();
			this.flags = new ArrayList<Flag>();

			Class<? extends Structure> currentClass = this.getClass();

			if (!COMPILED_PART_STRUCTURE.containsKey(currentClass) || !COMPILED_FLAG_STRUCTURE.containsKey(currentClass)) {
				compile();
			}

			List<Field> partFields = COMPILED_PART_STRUCTURE.get(currentClass);
			List<Field> flagFields = COMPILED_FLAG_STRUCTURE.get(currentClass);

			for (Field field : partFields) {
				try {
					Part<?> part = (Part<?>) field.get(this);

					this.parts.add(part);

				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			for (Field field : flagFields) {
				try {
					Flag flag = (Flag) field.get(this);

					this.flags.add(flag);

				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void readFlag(byte flagByte) {
		mappingPart();

		for (Flag flag : flags) {
			if ((flagByte & flag.getByteFlag()) != 0) {
				flag.setFlag(true);
			}
		}
	}

	public byte writeFlag() {
		mappingPart();
		
		byte flagByte = 0x0;

		for (Flag flag : flags) {
			if (flag.isFlag()) {
				flagByte |= flag.getByteFlag();
			}
		}

		return flagByte;
	}

	public void readPart(HttpInputStream httpInputStream) {
		mappingPart();

		for (Part<?> part : parts) {
			try {
				part.tryRead(httpInputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		processPostRead();
	}

	public void writePart(HttpOutputStream httpOutputStream) {
		mappingPart();

		processPreWrite();

		for (Part<?> part : parts) {
			try {
				part.tryWrite(httpOutputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public int getPartLength() {
		mappingPart();

		int length = 0;

		for (Part<?> part : parts) {
			length += part.getLength();
		}

		return length;
	}

	public void compile() {
		Class<? extends Structure> currentClass = this.getClass();

		Field[] declaredFields = currentClass.getDeclaredFields();

		List<Field> partFields = new LinkedList<Field>();
		List<Field> flagFields = new LinkedList<Field>();

		for (Field field : declaredFields) {
			if (Flag.class.isAssignableFrom(field.getType()) || field.getType() == Flag.class) {
				field.setAccessible(true);
				flagFields.add(field);
			}
			if (Part.class.isAssignableFrom(field.getType()) || field.getType() == Part.class) {
				field.setAccessible(true);
				partFields.add(field);
			}
		}

		COMPILED_PART_STRUCTURE.put(currentClass, partFields);
		COMPILED_FLAG_STRUCTURE.put(currentClass, flagFields);
	}

	public abstract void processPostRead();
	
	public abstract void processPreWrite();
	
	public List<Part<?>> getParts() {
		return parts;
	}

	public List<Flag> getFlags() {
		return flags;
	}
}
