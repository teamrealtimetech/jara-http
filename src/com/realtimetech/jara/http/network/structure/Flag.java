package com.realtimetech.jara.http.network.structure;

public class Flag {
	private byte byteFlag;

	private boolean flag;

	public Flag(byte byteFlag) {
		this.byteFlag = byteFlag;
		this.flag = false;
	}

	public byte getByteFlag() {
		return byteFlag;
	}
	
	public boolean isFlag() {
		return flag;
	}
	
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	public Flag reverse() {
		Flag flag2 = new Flag(byteFlag);
		flag2.setFlag(!flag);
		return flag2;
	}
}
