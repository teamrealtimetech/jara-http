package com.realtimetech.jara.http.network.structure.part;

import java.io.IOException;

import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;
import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;

public class UInt32Part extends NumberPart<Long>{
	public UInt32Part(Structure structure) {
		super(structure, 0L, null);
	}

	public UInt32Part(Structure structure, Flag flag) {
		super(structure, 0L, flag);
	}

	public UInt32Part(Structure structure, Long value) {
		super(structure, value, null);
	}

	public UInt32Part(Structure structure, Long value, Flag flag) {
		super(structure, value, flag);
	}

	@Override
	public void write(HttpOutputStream httpOutputStream, Long value) throws IOException {
		httpOutputStream.writeUnsigned32BitInteger(value);
	}

	@Override
	public Long read(HttpInputStream httpInputStream) throws IOException {
		return httpInputStream.readUnsigned32BitInteger();
	}

	@Override
	public int length() {
		return 4;
	}
}
