package com.realtimetech.jara.http.network.structure.part;

import java.io.IOException;

import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;
import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Part;
import com.realtimetech.jara.http.network.structure.Structure;

public class ByteArrayPart extends Part<byte[]> {
	private NumberPart<?> lengthPart;
	
	public ByteArrayPart(Structure structure, NumberPart<?> lengthPart) {
		this(structure, null, lengthPart);
		this.lengthPart = lengthPart;
	}

	public ByteArrayPart(Structure structure, Flag flag, NumberPart<?> lengthPart) {
		super(structure, null, flag);
		this.lengthPart = lengthPart;
	}
	
	public NumberPart<?> getLengthPart() {
		return lengthPart;
	}
	
	@Override
	public void write(HttpOutputStream httpOutputStream, byte[] value) throws IOException {
		httpOutputStream.write(value, 0, value.length);
	}

	@Override
	public byte[] read(HttpInputStream httpInputStream) throws IOException {
		return httpInputStream.readBytes(lengthPart.getNumber().intValue());
	}

	@Override
	public int length() {
		if(getValue() == null) {
			return lengthPart.getNumber().intValue();
		}else {
			return getValue().length;
		}
	}
}
