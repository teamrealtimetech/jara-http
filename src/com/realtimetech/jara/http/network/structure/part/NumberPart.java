package com.realtimetech.jara.http.network.structure.part;

import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Part;
import com.realtimetech.jara.http.network.structure.Structure;

public abstract class NumberPart<T> extends Part<T> {
	public NumberPart(Structure structure, T value, Flag flag) {
		super(structure, value, flag);
	}
	
	public Number getNumber() {
		return (Number) getValue();
	}
}
