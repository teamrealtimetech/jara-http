package com.realtimetech.jara.http.network.structure.part;

import java.io.IOException;

import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;
import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;

public class Int8Part extends NumberPart<Byte> {
	public Int8Part(Structure structure) {
		super(structure, (byte) 0, null);
	}

	public Int8Part(Structure structure, Flag flag) {
		super(structure, (byte) 0, flag);
	}

	public Int8Part(Structure structure, Byte value) {
		super(structure, value, null);
	}

	public Int8Part(Structure structure, Byte value, Flag flag) {
		super(structure, value, flag);
	}

	@Override
	public void write(HttpOutputStream httpOutputStream, Byte value) throws IOException {
		httpOutputStream.write(value);
	}

	@Override
	public Byte read(HttpInputStream httpInputStream) throws IOException {
		return (byte) httpInputStream.read();
	}

	@Override
	public int length() {
		return 1;
	}
}
