package com.realtimetech.jara.http.network.structure.part;

import java.io.IOException;

import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;
import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;

public class Int16Part extends NumberPart<Integer> {
	public Int16Part(Structure structure) {
		super(structure, 0, null);
	}

	public Int16Part(Structure structure, Flag flag) {
		super(structure, 0, flag);
	}

	public Int16Part(Structure structure, Integer value) {
		super(structure, value, null);
	}

	public Int16Part(Structure structure, Integer value, Flag flag) {
		super(structure, value, flag);
	}

	@Override
	public void write(HttpOutputStream httpOutputStream, Integer value) throws IOException {
		httpOutputStream.write16BitInteger(value);
	}

	@Override
	public Integer read(HttpInputStream httpInputStream) throws IOException {
		return httpInputStream.read16BitInteger();
	}

	@Override
	public int length() {
		return 2;
	}
}
