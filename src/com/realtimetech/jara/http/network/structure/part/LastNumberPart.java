package com.realtimetech.jara.http.network.structure.part;

import java.io.IOException;
import java.util.List;

import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;
import com.realtimetech.jara.http.network.structure.Part;
import com.realtimetech.jara.http.network.structure.Structure;

public class LastNumberPart extends NumberPart<Long> {
	public LastNumberPart(Structure structure) {
		super(structure, 0L, null);
	}

	@Override
	public void write(HttpOutputStream httpOutputStream, Long value) throws IOException {

	}

	@Override
	public Long read(HttpInputStream httpInputStream) throws IOException {
		return (long) httpInputStream.getLength();
	}

	@Override
	public int length() {
		return 0;
	}

	@Override
	public Number getNumber() {
		long length = (long) super.getNumber();
		
		List<Part<?>> parts = this.getStructure().getParts();

		for (Part<?> part : parts) {
			if (part instanceof ByteArrayPart) {
				ByteArrayPart byteArrayPart = (ByteArrayPart) part;

				if (byteArrayPart.getLengthPart() != this) {
					length -= part.getLength();
				}
			} else {
				length -= part.getLength();
			}
		}

		return length;
	}
}