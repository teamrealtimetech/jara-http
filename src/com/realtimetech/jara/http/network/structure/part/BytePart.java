package com.realtimetech.jara.http.network.structure.part;

import java.io.IOException;

import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;
import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;

public class BytePart extends NumberPart<Byte> {
	public BytePart(Structure structure) {
		super(structure, (byte) 0, null);
	}

	public BytePart(Structure structure, Flag flag) {
		super(structure, (byte) 0, flag);
	}

	public BytePart(Structure structure, Byte value) {
		super(structure, value, null);
	}

	public BytePart(Structure structure, Byte value, Flag flag) {
		super(structure, value, flag);
	}

	@Override
	public void write(HttpOutputStream httpOutputStream, Byte value) throws IOException {
		httpOutputStream.write(value.byteValue());
	}

	@Override
	public Byte read(HttpInputStream httpInputStream) throws IOException {
		return (byte) httpInputStream.read();
	}

	@Override
	public int length() {
		return 1;
	}
}
