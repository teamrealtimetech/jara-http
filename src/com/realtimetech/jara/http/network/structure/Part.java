package com.realtimetech.jara.http.network.structure;

import java.io.IOException;

import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;

public abstract class Part<T> {
	private T value;
	private Flag flag;
	private Structure structure;

	public Part(Structure structure, T value, Flag flag) {
		this.structure = structure;
		this.flag = flag;
		this.value = value;
	}

	public Structure getStructure() {
		return structure;
	}

	public int getLength() {
		if ((flag == null || flag.isFlag()) && value != null) {
			return length();
		} else {
			return 0;
		}
	}

	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}

	public void tryWrite(HttpOutputStream httpOutputStream) throws IOException {
		if ((flag == null || flag.isFlag()) && value != null) {
			write(httpOutputStream, value);
		}
	}

	public void tryRead(HttpInputStream httpInputStream) throws IOException {
		if (flag == null || flag.isFlag()) {
			value = read(httpInputStream);
		}
	}

	abstract public void write(HttpOutputStream httpOutputStream, T value) throws IOException;

	abstract public T read(HttpInputStream httpInputStream) throws IOException;

	abstract public int length();
}
