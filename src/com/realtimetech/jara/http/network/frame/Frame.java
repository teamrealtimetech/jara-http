package com.realtimetech.jara.http.network.frame;

import java.io.IOException;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;
import com.realtimetech.jara.http.network.frame.impl.structures.setting.SettingType;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;
import com.realtimetech.jara.http.network.structure.Structure;

/**
 * HTTP2 프로토콜의 기본 통신 단위인 Frame의 추상화 Class이다.
 * 
 * @author ParkJeongHwan
 */
public abstract class Frame<T extends Structure> {
	private Stream stream;
	private boolean readOnly;

	private T payloadStructure;

	public Frame(Stream stream) {
		this.readOnly = false;
		this.stream = stream;
		this.payloadStructure = createNewStructure();
	}

	public Frame(Stream stream, T payloadStructure) {
		this.readOnly = true;
		this.stream = stream;
		this.payloadStructure = payloadStructure;
	}

	public Stream getStream() {
		return stream;
	}

	public T getPayloadStructure() {
		return payloadStructure;
	}

	public void writeFrame(HttpOutputStream httpOutputStream) throws IOException{
		if (!readOnly) {
			int length = payloadStructure.getPartLength();

			if (length >= stream.getConnection().getRemoteSettings().get(SettingType.SETTINGS_MAX_FRAME_SIZE)) {
				stream.getConnection().sendProtocolError(new ProtocolErrorException(ProtocolErrorType.FRAME_SIZE_ERROR, stream, "Frame size is so big."));
			} else {
				long streamId = 0x7FFFFFFF & this.stream.getId();

				httpOutputStream.write24BitInteger(length);
				httpOutputStream.write(getFrameType().getType());

				httpOutputStream.write(payloadStructure.writeFlag());
				httpOutputStream.writeUnsigned32BitInteger(streamId);

				payloadStructure.writePart(httpOutputStream);
			}
		}
	}

	public void readFrame(byte frameFlag, HttpInputStream httpInputStream) {
		if (readOnly) {
			int length = httpInputStream.getLength();
			
			if (length >= stream.getConnection().getRemoteSettings().get(SettingType.SETTINGS_MAX_FRAME_SIZE)) {
				stream.getConnection().sendProtocolError(new ProtocolErrorException(ProtocolErrorType.FRAME_SIZE_ERROR, stream, "Frame size is so big."));
			} else {
				payloadStructure.readFlag(frameFlag);

				payloadStructure.readPart(httpInputStream);
			}
		}
	}

	public FrameOrder preOrderReadFrame(byte frameFlag, HttpInputStream httpInputStream) throws IOException {
		if (readOnly) {
			return new FrameOrder(this, httpInputStream, frameFlag);
		}

		return null;
	}

	/**
	 * Frame 추상화 클래스의 구현체의 FrameType을 가져오는 메서드이다.
	 * 
	 * @return 해당하는 FrameType을 반환한다.
	 */
	public abstract FrameType getFrameType();

	public abstract T createNewStructure();

	public abstract void processReadFrame(StreamState streamState) throws ProtocolErrorException, IOException;

	public abstract void processWriteFrame(StreamState streamState) throws ProtocolErrorException;

}
