package com.realtimetech.jara.http.network.frame.impl.structures;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.structure.Structure;
import com.realtimetech.jara.http.network.structure.part.ByteArrayPart;
import com.realtimetech.jara.http.network.structure.part.LastNumberPart;
import com.realtimetech.jara.http.network.structure.part.UInt32Part;

public class GoAwayStructure extends Structure {
	private LastNumberPart lastNumberPart = new LastNumberPart(this);
	private UInt32Part lastStreamId = new UInt32Part(this);
	private UInt32Part errorCode = new UInt32Part(this);
	private ByteArrayPart debugData = new ByteArrayPart(this, lastNumberPart);

	private ProtocolErrorType errorType = ProtocolErrorType.NO_ERROR;

	@Override
	public void processPostRead() {
		this.errorType = ProtocolErrorType.tryParse(errorCode.getValue().byteValue());
	}

	@Override
	public void processPreWrite() {
		errorCode.setValue((long)errorType.getType());
	}

	public UInt32Part getLastStreamId() {
		return lastStreamId;
	}

	public ProtocolErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(ProtocolErrorType errorType) {
		this.errorType = errorType;
	}
	
	public ByteArrayPart getDebugData() {
		return debugData;
	}
}
