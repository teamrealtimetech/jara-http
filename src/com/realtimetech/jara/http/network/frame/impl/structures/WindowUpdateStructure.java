package com.realtimetech.jara.http.network.frame.impl.structures;

import com.realtimetech.jara.http.network.structure.Structure;
import com.realtimetech.jara.http.network.structure.part.UInt32Part;

public class WindowUpdateStructure extends Structure{
	private UInt32Part windowSize = new UInt32Part(this);

	@Override
	public void processPostRead() {
		
	}

	@Override
	public void processPreWrite() {
		
	}

	public UInt32Part getWindowSize() {
		return windowSize;
	}
}
