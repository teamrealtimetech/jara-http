package com.realtimetech.jara.http.network.frame.impl.structures;

import java.util.HashMap;

import com.realtimetech.jara.http.network.frame.impl.structures.setting.SettingType;
import com.realtimetech.jara.http.network.frame.impl.structures.setting.Settings;
import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;
import com.realtimetech.jara.http.network.structure.part.ByteArrayPart;
import com.realtimetech.jara.http.network.structure.part.LastNumberPart;

public class SettingStructure extends Structure {
	private Flag ack = new Flag((byte) 0x1);

	private LastNumberPart lastNumberPart = new LastNumberPart(this);
	private ByteArrayPart settingBytes = new ByteArrayPart(this, ack.reverse(), lastNumberPart);

	private Settings settings = new Settings(new HashMap<>());

	@Override
	public void processPostRead() {
		settings.clear();

		if(!ack.isFlag()) {
			byte[] value = settingBytes.getValue();
			int index = 0;

			while (index < value.length) {
				int settingId;
				int settingValue;

				settingId = value[index++] << 8;
				settingId += value[index++];

				settingValue = value[index++] << 24;
				settingValue += value[index++] << 16;
				settingValue += value[index++] << 8;
				settingValue += value[index++];

				settings.set(SettingType.tryParse(settingId), settingValue);
			}
		}
	}

	@Override
	public void processPreWrite() {
		if(!ack.isFlag()) {
			byte[] bytes = new byte[settings.getTypes().size() * 6];
			int index = 0;

			for (SettingType settingType : settings.getTypes()) {
				bytes[index++] = (byte) ((settingType.getType() & 0x0000FF00) >> 8);
				bytes[index++] = (byte) ((settingType.getType() & 0x000000FF) >> 0);

				int value = settings.get(settingType);
				bytes[index++] = (byte) ((value & 0xFF000000) >> 24);
				bytes[index++] = (byte) ((value & 0x00FF0000) >> 16);
				bytes[index++] = (byte) ((value & 0x0000FF00) >> 8);
				bytes[index++] = (byte) ((value & 0x000000FF) >> 0);
			}
			
			settingBytes.setValue(bytes);
		}else {

			settingBytes.setValue(new byte[0]);
		}
	}

	public Flag getAck() {
		return ack;
	}

	public LastNumberPart getLastNumberPart() {
		return lastNumberPart;
	}
	
	public ByteArrayPart getSettingBytes() {
		return settingBytes;
	}
	
	public Settings getSettings() {
		return settings;
	}
}
