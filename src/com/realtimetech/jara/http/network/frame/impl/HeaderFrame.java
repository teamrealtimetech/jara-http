package com.realtimetech.jara.http.network.frame.impl;

import java.io.IOException;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.HeaderStructure;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class HeaderFrame extends Frame<HeaderStructure> {
	public HeaderFrame(Stream stream) {
		super(stream);
	}

	public HeaderFrame(Stream stream, HeaderStructure headerStructure) {
		super(stream, headerStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.HEADER_FRAME;
	}

	@Override
	public HeaderStructure createNewStructure() {
		return new HeaderStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case HALF_CLOSED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is half closed.");
		case CLOSED:
			if (getStream().isRstStatus()) {
				throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is closed.");
			} else {
				throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is closed.");
			}
		case IDLE:
			if (isEndStream()) {
				getStream().setState(StreamState.HALF_CLOSED_REMOTE);
			} else {
				getStream().setState(StreamState.OPEN);
			}
			break;
		case RESERVED_REMOTE:
			getStream().setState(StreamState.HALF_CLOSED_LOCAL);

			break;
		case OPEN:
			if (isEndStream()) {
				getStream().setState(StreamState.HALF_CLOSED_REMOTE);
			}

			break;
		case HALF_CLOSED_LOCAL:
			if (isEndStream()) {
				getStream().setState(StreamState.CLOSED);
			}

			break;
		}

		try {
			getStream().getHttpRequest().appendHeaderBytes(getEncodedData(), isEndHeaders(), isEndStream());
		} catch (IOException e) {
			throw new ProtocolErrorException(ProtocolErrorType.COMPRESSION_ERROR, getStream(), "Error occurred during header writing.");
		}
	}

	@Override
	public void processWriteFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case HALF_CLOSED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is half closed.");
		case CLOSED:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is closed.");
		case RESERVED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is reserved.");
		case IDLE:
			if (isEndStream()) {
				getStream().setState(StreamState.HALF_CLOSED_REMOTE);
			} else {
				getStream().setState(StreamState.OPEN);
			}
			break;
		case RESERVED_LOCAL:
			getStream().setState(StreamState.HALF_CLOSED_REMOTE);

			break;
		case OPEN:
			if (isEndStream()) {
				getStream().setState(StreamState.HALF_CLOSED_LOCAL);
			}

			break;
		case HALF_CLOSED_REMOTE:
			if (isEndStream()) {
				getStream().setState(StreamState.CLOSED);
			}

			break;
		}
	}

	public boolean isEndStream() {
		return getPayloadStructure().getEndStream().isFlag();
	}

	public HeaderFrame setEndStream(boolean endStream) {
		getPayloadStructure().getEndStream().setFlag(endStream);

		return this;
	}

	public boolean isEndHeaders() {
		return getPayloadStructure().getEndHeaders().isFlag();
	}

	public HeaderFrame setEndHeaders(boolean endHeaders) {
		getPayloadStructure().getEndHeaders().setFlag(endHeaders);

		return this;
	}

	public boolean isPadded() {
		return getPayloadStructure().getPadded().isFlag();
	}

	public HeaderFrame setPadded(boolean padded) {
		getPayloadStructure().getPadded().setFlag(padded);

		return this;
	}

	public boolean isPriority() {
		return getPayloadStructure().getPriority().isFlag();
	}

	public HeaderFrame setPriority(boolean priority) {
		getPayloadStructure().getPriority().setFlag(priority);

		return this;
	}

	public byte getPaddedLength() {
		return getPayloadStructure().getPaddedLength().getValue();
	}

	public HeaderFrame setPaddedLength(byte length) {
		getPayloadStructure().getPaddedLength().setValue(length);
		getPayloadStructure().getPadding().setValue(new byte[length]);

		return this;
	}

	public long getDependencyStreamId() {
		return getPayloadStructure().getDependencyStreamId().getValue();
	}

	public HeaderFrame setDependencyStreamId(long dependencyStreamId) {
		getPayloadStructure().getDependencyStreamId().setValue(dependencyStreamId);

		return this;
	}

	public byte getWeight() {
		return getPayloadStructure().getWeight().getValue().byteValue();
	}

	public HeaderFrame setWeight(byte weight) {
		getPayloadStructure().getWeight().setValue(weight);

		return this;
	}

	public byte[] getEncodedData() {
		return getPayloadStructure().getEncodedData().getValue();
	}

	public HeaderFrame setEncodedData(byte[] encodedData) {
		getPayloadStructure().getEncodedData().setValue(encodedData);

		return this;
	}
}
