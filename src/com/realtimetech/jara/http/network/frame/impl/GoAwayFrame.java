package com.realtimetech.jara.http.network.frame.impl;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.GoAwayStructure;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class GoAwayFrame extends Frame<GoAwayStructure> {
	public GoAwayFrame(Stream stream) {
		super(stream);
	}

	public GoAwayFrame(Stream stream, GoAwayStructure goAwayStructure) {
		super(stream, goAwayStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.GOAWAY_FRAME;
	}

	@Override
	public GoAwayStructure createNewStructure() {
		return new GoAwayStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		
		if (getStream().getId() != 0) {
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Go away connection only can in 0 stream.");
		} else {
			getStream().getConnection().deactive();
		}
	}

	@Override
	public void processWriteFrame(StreamState streamState) throws ProtocolErrorException {
	}

	public ProtocolErrorType getErrorType() {
		return getPayloadStructure().getErrorType();
	}

	public GoAwayFrame setErrorType(ProtocolErrorType protocolErrorType) {
		getPayloadStructure().setErrorType(protocolErrorType);

		return this;
	}

	public byte[] getDebugData() {
		return getPayloadStructure().getDebugData().getValue();
	}

	public GoAwayFrame setDebugData(byte[] bytes) {
		getPayloadStructure().getDebugData().setValue(bytes);

		return this;
	}
}
