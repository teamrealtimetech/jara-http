package com.realtimetech.jara.http.network.frame.impl.structures;

import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;
import com.realtimetech.jara.http.network.structure.part.ByteArrayPart;
import com.realtimetech.jara.http.network.structure.part.Int8Part;
import com.realtimetech.jara.http.network.structure.part.LastNumberPart;
import com.realtimetech.jara.http.network.structure.part.UInt32Part;

public class HeaderStructure extends Structure {
	private Flag endStream = new Flag((byte) 0x1);
	private Flag endHeaders = new Flag((byte) 0x4);
	private Flag padded = new Flag((byte) 0x8);
	private Flag priority = new Flag((byte) 0x20);

	private LastNumberPart lastNumberPart = new LastNumberPart(this);
	private Int8Part paddedLength = new Int8Part(this, padded);
	private UInt32Part dependencyStreamId = new UInt32Part(this, priority);
	private Int8Part weight = new Int8Part(this, priority);
	private ByteArrayPart encodedData = new ByteArrayPart(this, lastNumberPart);
	private ByteArrayPart padding = new ByteArrayPart(this, padded, this.paddedLength);

	@Override
	public void processPostRead() {
		
	}

	@Override
	public void processPreWrite() {
		
	}

	public Flag getEndStream() {
		return endStream;
	}
	
	public Flag getEndHeaders() {
		return endHeaders;
	}
	
	public Flag getPadded() {
		return padded;
	}
	public Flag getPriority() {
		return priority;
	}
	
	public LastNumberPart getLastNumberPart() {
		return lastNumberPart;
	}
	
	public Int8Part getPaddedLength() {
		return paddedLength;
	}
	
	public UInt32Part getDependencyStreamId() {
		return dependencyStreamId;
	}
	
	public Int8Part getWeight() {
		return weight;
	}
	
	public ByteArrayPart getEncodedData() {
		return encodedData;
	}
	
	public ByteArrayPart getPadding() {
		return padding;
	}
}
