package com.realtimetech.jara.http.network.frame.impl.structures.setting;

import java.util.HashMap;
import java.util.Set;

public class Settings {
	private HashMap<SettingType, Integer> settingValues;

	public Settings() {
		this.settingValues = new HashMap<SettingType, Integer>();

		this.set(SettingType.SETTINGS_HEADER_TABLE_SIZE, 4096).set(SettingType.SETTINGS_ENABLE_PUSH, 1).set(SettingType.SETTINGS_MAX_CONCURRENT_STREAMS, 100).set(SettingType.SETTINGS_INITIAL_WINDOW_SIZE, 65535).set(SettingType.SETTINGS_MAX_FRAME_SIZE, 16384).set(SettingType.SETTINGS_MAX_HEADER_LIST_SIZE, 32);
	}

	public Settings(HashMap<SettingType, Integer> settingValues) {
		this.settingValues = settingValues;
	}

	public Settings clear() {
		settingValues.clear();

		return this;
	}

	public Settings set(SettingType type, int value) {
		settingValues.put(type, value);

		return this;
	}

	public int get(SettingType type) {
		return settingValues.get(type);
	}

	public Set<SettingType> getTypes() {
		return settingValues.keySet();
	}

	public Settings mergeWith(Settings targetSettings) {
		for (SettingType settingType : targetSettings.getTypes()) {
			set(settingType, targetSettings.get(settingType));
		}

		return this;
	}
}
