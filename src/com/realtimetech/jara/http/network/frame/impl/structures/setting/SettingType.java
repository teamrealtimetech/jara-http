package com.realtimetech.jara.http.network.frame.impl.structures.setting;

public enum SettingType {
	SETTINGS_HEADER_TABLE_SIZE((int) 0x1),
	SETTINGS_ENABLE_PUSH((int) 0x2),
	SETTINGS_MAX_CONCURRENT_STREAMS((int) 0x3),
	SETTINGS_INITIAL_WINDOW_SIZE((int) 0x4),
	SETTINGS_MAX_FRAME_SIZE((int) 0x5),
	SETTINGS_MAX_HEADER_LIST_SIZE((int) 0x6);

	private int type;

	SettingType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public static SettingType tryParse(int type) {
		for (SettingType settingType : SettingType.values()) {
			if (settingType.getType() == type) {
				return settingType;
			}
		}

		return null;
	}
}
