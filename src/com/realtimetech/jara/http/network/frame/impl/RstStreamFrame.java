package com.realtimetech.jara.http.network.frame.impl;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.RstStreamStructure;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class RstStreamFrame extends Frame<RstStreamStructure> {
	public RstStreamFrame(Stream stream) {
		super(stream);
	}

	public RstStreamFrame(Stream stream, RstStreamStructure rstStreamStructure) {
		super(stream, rstStreamStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.RST_STREAM_FRAME;
	}

	@Override
	public RstStreamStructure createNewStructure() {
		return new RstStreamStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case CLOSED:
			if (getStream().isRstStatus()) {
				throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is closed.");
			} else {
				throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is closed.");
			}
		case RESERVED_LOCAL:
			getStream().setState(StreamState.CLOSED);
			getStream().setRstStatus(true);

			break;
		case RESERVED_REMOTE:
			getStream().setState(StreamState.CLOSED);
			getStream().setRstStatus(true);

			break;
		case HALF_CLOSED_LOCAL:
			getStream().setState(StreamState.CLOSED);
			getStream().setRstStatus(true);

			break;
		case HALF_CLOSED_REMOTE:
			getStream().setState(StreamState.CLOSED);
			getStream().setRstStatus(true);

			break;
		case IDLE:
			// WHY HAS BEEN ERROR?
			break;
		case OPEN:
			// WHY HAS BEEN ERROR?
			break;
		}
		
	}

	@Override
	public void processWriteFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case CLOSED:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is closed.");
		case RESERVED_LOCAL:
			getStream().setState(StreamState.CLOSED);
			getStream().setRstStatus(true);
			
			break;
		case RESERVED_REMOTE:
			getStream().setState(StreamState.CLOSED);
			getStream().setRstStatus(true);
			
			break;
		case HALF_CLOSED_LOCAL:
			getStream().setState(StreamState.CLOSED);
			getStream().setRstStatus(true);
			
			break;
		case HALF_CLOSED_REMOTE:
			getStream().setState(StreamState.CLOSED);
			getStream().setRstStatus(true);
			
			break;
		default:
			break;
		}
	}

	public ProtocolErrorType getErrorType() {
		return getPayloadStructure().getErrorType();
	}

	public RstStreamFrame setErrorType(ProtocolErrorType protocolErrorType) {
		getPayloadStructure().setErrorType(protocolErrorType);

		return this;
	}
}
