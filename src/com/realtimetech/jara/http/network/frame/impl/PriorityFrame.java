package com.realtimetech.jara.http.network.frame.impl;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.PriorityStructure;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class PriorityFrame extends Frame<PriorityStructure> {
	public PriorityFrame(Stream stream) {
		super(stream);
	}
	
	public PriorityFrame(Stream stream, PriorityStructure priorityStructure) {
		super(stream, priorityStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.PRIORITY_FRAME;
	}

	@Override
	public PriorityStructure createNewStructure() {
		return new PriorityStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		default:
			break;
		}
	}

	@Override
	public void processWriteFrame(StreamState streamState) {
	}

	public long getDependencyStreamId() {
		return getPayloadStructure().getDependencyStreamId().getValue();
	}

	public PriorityFrame setDependencyStreamId(long dependencyStreamId) {
		getPayloadStructure().getDependencyStreamId().setValue(dependencyStreamId);
		
		return this;
	}

	public byte getWeight() {
		return getPayloadStructure().getWeight().getValue().byteValue();
	}

	public PriorityFrame setWeight(byte weight) {
		getPayloadStructure().getWeight().setValue(weight);
		
		return this;
	}
}
