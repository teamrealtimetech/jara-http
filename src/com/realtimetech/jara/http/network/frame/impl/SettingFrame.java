package com.realtimetech.jara.http.network.frame.impl;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.SettingStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.setting.Settings;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class SettingFrame extends Frame<SettingStructure> {
	public SettingFrame(Stream stream) {
		super(stream);
	}

	public SettingFrame(Stream stream, SettingStructure settingStructure) {
		super(stream, settingStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.SETTING_FRAME;
	}

	@Override
	public SettingStructure createNewStructure() {
		return new SettingStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case RESERVED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case HALF_CLOSED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is half closed.");
		case CLOSED:
			if (getStream().isRstStatus()) {
				throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is closed.");
			} else {
				throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is closed.");
			}
		default:
			break;
		}

		if (getStream().getId() != 0) {
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Setting connection only can in 0 stream.");
		} else {
			getStream().getConnection().getRemoteSettings().mergeWith(this.getSettings());
			getStream().getConnection().sendFrame(new SettingFrame(getStream()).setAck(true));
		}
	}

	@Override
	public void processWriteFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is reserved.");
		case HALF_CLOSED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is reserved.");
		case CLOSED:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is reserved.");
		default:
			break;
		}
	}

	public boolean isAck() {
		return getPayloadStructure().getAck().isFlag();
	}

	public SettingFrame setAck(boolean ack) {
		getPayloadStructure().getAck().setFlag(ack);

		return this;
	}

	public SettingFrame setSettings(Settings targetSettings) {
		getPayloadStructure().getSettings().clear().mergeWith(targetSettings);
		
		return this;
	}

	public Settings getSettings() {
		return getPayloadStructure().getSettings();
	}
}
