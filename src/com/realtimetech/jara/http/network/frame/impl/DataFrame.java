package com.realtimetech.jara.http.network.frame.impl;

import java.io.IOException;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.DataStructure;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class DataFrame extends Frame<DataStructure> {
	public DataFrame(Stream stream) {
		super(stream);
	}

	public DataFrame(Stream stream, DataStructure dataStructure) {
		super(stream, dataStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.DATA_FRAME;
	}

	@Override
	public DataStructure createNewStructure() {
		return new DataStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case IDLE:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Required header frame first.");
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case RESERVED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case HALF_CLOSED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is half closed.");
		case CLOSED:
			if (getStream().isRstStatus()) {
				throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is closed.");
			} else {
				throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is closed.");
			}
		case OPEN:
			if (isEndStream()) {
				getStream().setState(StreamState.HALF_CLOSED_REMOTE);
			}
			
			break;
		case HALF_CLOSED_LOCAL:
			if (isEndStream()) {
				getStream().setState(StreamState.CLOSED);
			}
			
			break;
		}
		
		try {
			getStream().getHttpRequest().appendDataBytes(getData(), isEndStream());
		} catch (IOException e) {
			throw new ProtocolErrorException(ProtocolErrorType.COMPRESSION_ERROR, getStream(), "Error occurred during header writing.");
		}
	}

	@Override
	public void processWriteFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case IDLE:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Sending frame before header frame.");
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is reserved.");
		case HALF_CLOSED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is half closed.");
		case CLOSED:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is closed.");
		case OPEN:
			if (isEndStream()) {
				getStream().setState(StreamState.HALF_CLOSED_LOCAL);
			}
			
			break;
		case HALF_CLOSED_REMOTE:
			if (isEndStream()) {
				getStream().setState(StreamState.CLOSED);
			}
			
			break;
		default:
			
			break;
		}
	}

	public boolean isEndStream() {
		return getPayloadStructure().getEndStream().isFlag();
	}

	public DataFrame setEndStream(boolean endStream) {
		getPayloadStructure().getEndStream().setFlag(endStream);

		return this;
	}

	public boolean isPadded() {
		return getPayloadStructure().getPadded().isFlag();
	}

	public DataFrame setPadded(boolean padded) {
		getPayloadStructure().getPadded().setFlag(padded);

		return this;
	}

	public byte getPaddedLength() {
		return getPayloadStructure().getPaddedLength().getValue().byteValue();
	}

	public DataFrame setPaddedLength(byte length) {
		getPayloadStructure().getPaddedLength().setValue(length);
		getPayloadStructure().getPadding().setValue(new byte[length]);

		return this;
	}

	public byte[] getData() {
		return getPayloadStructure().getData().getValue();
	}

	public DataFrame setData(byte[] data) {
		getPayloadStructure().getData().setValue(data);

		return this;
	}
}
