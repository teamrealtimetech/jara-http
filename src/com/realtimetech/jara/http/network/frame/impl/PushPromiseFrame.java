package com.realtimetech.jara.http.network.frame.impl;

import java.io.IOException;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.PushPromiseStructure;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class PushPromiseFrame extends Frame<PushPromiseStructure> {
	public PushPromiseFrame(Stream stream) {
		super(stream);
	}
	
	public PushPromiseFrame(Stream stream, PushPromiseStructure pushPromiseStructure) {
		super(stream, pushPromiseStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.PUSH_PROMISE_FRAME;
	}

	@Override
	public PushPromiseStructure createNewStructure() {
		return new PushPromiseStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case RESERVED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case OPEN:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Promise frame is only can idle status.");
		case HALF_CLOSED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Promise frame is only can idle status.");
		case HALF_CLOSED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is half closed.");
		case CLOSED:
			if(getStream().isRstStatus()) {
				throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is closed.");
			}else {
				throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is closed.");
			}
		case IDLE: 
			getStream().setState(StreamState.RESERVED_REMOTE);
			
			break;
		}
		
		try {
			if (!getStream().getHttpRequest().isHasHeaders()) {
				getStream().getHttpRequest().appendHeaderBytes(getEncodedData(), isEndHeaders(), false);
			} else {
				throw new ProtocolErrorException(ProtocolErrorType.COMPRESSION_ERROR, getStream(), "Must header frame is first received than push promise.");
			}
		} catch (IOException e) {
			throw new ProtocolErrorException(ProtocolErrorType.COMPRESSION_ERROR, getStream(), "Error occurred during header writing.");
		}
	}

	@Override
	public void processWriteFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Promise frame is only can idle status.");
		case RESERVED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Promise frame is only can idle status.");
		case OPEN:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Promise frame is only can idle status.");
		case HALF_CLOSED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Promise frame is only can idle status.");
		case HALF_CLOSED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Promise frame is only can idle status.");
		case CLOSED: 
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Promise frame is only can idle status.");
		case IDLE:
			getStream().setState(StreamState.RESERVED_LOCAL);
			
			break;
		}
	}

	public boolean isEndHeaders() {
		return getPayloadStructure().getEndHeaders().isFlag();
	}

	public PushPromiseFrame setEndHeaders(boolean endHeaders) {
		getPayloadStructure().getEndHeaders().setFlag(endHeaders);

		return this;
	}

	public boolean isPadded() {
		return getPayloadStructure().getPadded().isFlag();
	}

	public PushPromiseFrame setPadded(boolean padded) {
		getPayloadStructure().getPadded().setFlag(padded);

		return this;
	}

	public byte getPaddedLength() {
		return getPayloadStructure().getPaddedLength().getValue();
	}

	public PushPromiseFrame setPaddedLength(byte length) {
		getPayloadStructure().getPaddedLength().setValue(length);
		getPayloadStructure().getPadding().setValue(new byte[length]);

		return this;
	}

	public long getPromisedStreamId() {
		return getPayloadStructure().getPromisedStreamId().getValue();
	}

	public PushPromiseFrame setPromisedStreamId(long dependencyStreamId) {
		getPayloadStructure().getPromisedStreamId().setValue(dependencyStreamId);

		return this;
	}

	public byte[] getEncodedData() {
		return getPayloadStructure().getEncodedData().getValue();
	}

	public PushPromiseFrame setEncodedData(byte[] encodedData) {
		getPayloadStructure().getEncodedData().setValue(encodedData);

		return this;
	}
}
