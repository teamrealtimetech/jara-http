package com.realtimetech.jara.http.network.frame.impl.structures;

import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;
import com.realtimetech.jara.http.network.structure.part.ByteArrayPart;
import com.realtimetech.jara.http.network.structure.part.LastNumberPart;

public class ContinuationStructure extends Structure {
	private Flag endHeaders = new Flag((byte) 0x1);

	private LastNumberPart lastNumberPart = new LastNumberPart(this);
	private ByteArrayPart encodedData = new ByteArrayPart(this, lastNumberPart);

	@Override
	public void processPostRead() {

	}

	@Override
	public void processPreWrite() {

	}

	public Flag getEndHeaders() {
		return endHeaders;
	}

	public ByteArrayPart getEncodedData() {
		return encodedData;
	}
}
