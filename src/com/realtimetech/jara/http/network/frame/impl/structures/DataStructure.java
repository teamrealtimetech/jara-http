package com.realtimetech.jara.http.network.frame.impl.structures;

import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;
import com.realtimetech.jara.http.network.structure.part.ByteArrayPart;
import com.realtimetech.jara.http.network.structure.part.Int8Part;
import com.realtimetech.jara.http.network.structure.part.LastNumberPart;

public class DataStructure extends Structure {
	private Flag endStream = new Flag((byte) 0x1);
	private Flag padded = new Flag((byte) 0x8);

	private LastNumberPart lastNumberPart = new LastNumberPart(this);
	private Int8Part paddedLength = new Int8Part(this, padded);
	private ByteArrayPart data = new ByteArrayPart(this, lastNumberPart);
	private ByteArrayPart padding = new ByteArrayPart(this, padded, this.paddedLength);

	@Override
	public void processPostRead() {
		
	}

	@Override
	public void processPreWrite() {
		
	}

	public Flag getEndStream() {
		return endStream;
	}

	public Flag getPadded() {
		return padded;
	}

	public LastNumberPart getLastNumberPart() {
		return lastNumberPart;
	}

	public Int8Part getPaddedLength() {
		return paddedLength;
	}

	public ByteArrayPart getData() {
		return data;
	}

	public ByteArrayPart getPadding() {
		return padding;
	}
}
