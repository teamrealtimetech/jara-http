package com.realtimetech.jara.http.network.frame.impl;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.WindowUpdateStructure;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class WindowUpdateFrame extends Frame<WindowUpdateStructure> {
	public WindowUpdateFrame(Stream stream) {
		super(stream);
	}

	public WindowUpdateFrame(Stream stream, WindowUpdateStructure windowUpdateStructure) {
		super(stream, windowUpdateStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.WINDOW_UPDATE_FRAME;
	}

	@Override
	public WindowUpdateStructure createNewStructure() {
		return new WindowUpdateStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case RESERVED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case CLOSED:
			if (getStream().isRstStatus()) {
				throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is closed.");
			} else {
				throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is closed.");
			}
		default:
			break;
		}
	}

	@Override
	public void processWriteFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is reserved.");
		case CLOSED:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is closed.");
		default:
			break;
		}
	}

	public long getWindowSize() {
		return getPayloadStructure().getWindowSize().getValue();
	}

	public WindowUpdateFrame setWindowSize(long windowSize) {
		getPayloadStructure().getWindowSize().setValue(windowSize);

		return this;
	}
}
