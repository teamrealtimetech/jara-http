package com.realtimetech.jara.http.network.frame.impl.structures;

import com.realtimetech.jara.http.network.structure.Flag;
import com.realtimetech.jara.http.network.structure.Structure;
import com.realtimetech.jara.http.network.structure.part.ByteArrayPart;
import com.realtimetech.jara.http.network.structure.part.Int8Part;
import com.realtimetech.jara.http.network.structure.part.LastNumberPart;
import com.realtimetech.jara.http.network.structure.part.UInt32Part;

public class PushPromiseStructure extends Structure {
	private Flag endHeaders = new Flag((byte) 0x1);
	private Flag padded = new Flag((byte) 0x8);

	private LastNumberPart lastNumberPart = new LastNumberPart(this);
	private Int8Part paddedLength = new Int8Part(this, padded);
	private UInt32Part promisedStreamId = new UInt32Part(this);
	private ByteArrayPart encodedData = new ByteArrayPart(this, lastNumberPart);
	private ByteArrayPart padding = new ByteArrayPart(this, padded, this.paddedLength);

	@Override
	public void processPostRead() {
		
	}

	@Override
	public void processPreWrite() {
		
	}

	public Flag getEndHeaders() {
		return endHeaders;
	}

	public Flag getPadded() {
		return padded;
	}

	public LastNumberPart getLastNumberPart() {
		return lastNumberPart;
	}

	public Int8Part getPaddedLength() {
		return paddedLength;
	}

	public UInt32Part getPromisedStreamId() {
		return promisedStreamId;
	}

	public ByteArrayPart getEncodedData() {
		return encodedData;
	}

	public ByteArrayPart getPadding() {
		return padding;
	}
}
