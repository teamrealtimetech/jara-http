package com.realtimetech.jara.http.network.frame.impl;

import java.io.IOException;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.structures.ContinuationStructure;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamState;

public class ContinuationFrame extends Frame<ContinuationStructure> {
	public ContinuationFrame(Stream stream) {
		super(stream);
	}

	public ContinuationFrame(Stream stream, ContinuationStructure continuationStructure) {
		super(stream, continuationStructure);
	}

	@Override
	public FrameType getFrameType() {
		return FrameType.CONTINUATION;
	}

	@Override
	public ContinuationStructure createNewStructure() {
		return new ContinuationStructure();
	}

	@Override
	public void processReadFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case RESERVED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is reserved.");
		case HALF_CLOSED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is half closed.");
		case CLOSED:
			if (getStream().isRstStatus()) {
				throw new ProtocolErrorException(ProtocolErrorType.STREAM_CLOSED, getStream(), "Stream is closed.");
			} else {
				throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, getStream(), "Stream is closed.");
			}
		case IDLE:
			getStream().setState(StreamState.OPEN);
			break;
		case RESERVED_REMOTE:
			getStream().setState(StreamState.HALF_CLOSED_LOCAL);

			break;
		case OPEN:
			break;
		case HALF_CLOSED_LOCAL:

			break;
		}

		try {
			if (!getStream().getHttpRequest().isHasHeaders()) {
				getStream().getHttpRequest().appendHeaderBytes(getEncodedData(), isEndHeaders(), false);
			} else {
				throw new ProtocolErrorException(ProtocolErrorType.COMPRESSION_ERROR, getStream(), "Must header frame is first received than continuation.");
			}
		} catch (IOException e) {
			throw new ProtocolErrorException(ProtocolErrorType.COMPRESSION_ERROR, getStream(), "Error occurred during header writing.");
		}
	}

	@Override
	public void processWriteFrame(StreamState streamState) throws ProtocolErrorException {
		switch (streamState) {
		case HALF_CLOSED_LOCAL:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is half closed.");
		case CLOSED:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is closed.");
		case RESERVED_REMOTE:
			throw new ProtocolErrorException(ProtocolErrorType.SERVER_RULE_ERROR, getStream(), "Stream is reserved.");
		case IDLE:
			getStream().setState(StreamState.OPEN);

			break;
		case RESERVED_LOCAL:
			getStream().setState(StreamState.HALF_CLOSED_REMOTE);

			break;
		case OPEN:
			break;
		case HALF_CLOSED_REMOTE:
			break;
		}
	}

	public boolean isEndHeaders() {
		return getPayloadStructure().getEndHeaders().isFlag();
	}

	public ContinuationFrame setEndHeaders(boolean endHeaders) {
		getPayloadStructure().getEndHeaders().setFlag(endHeaders);

		return this;
	}

	public byte[] getEncodedData() {
		return getPayloadStructure().getEncodedData().getValue();
	}

	public ContinuationFrame setEncodedData(byte[] encodedData) {
		getPayloadStructure().getEncodedData().setValue(encodedData);

		return this;
	}
}
