package com.realtimetech.jara.http.network.frame.impl.structures;

import com.realtimetech.jara.http.network.structure.Structure;
import com.realtimetech.jara.http.network.structure.part.Int8Part;
import com.realtimetech.jara.http.network.structure.part.UInt32Part;

public class PriorityStructure extends Structure {
	private UInt32Part dependencyStreamId = new UInt32Part(this);
	private Int8Part weight = new Int8Part(this);

	@Override
	public void processPostRead() {
		
	}

	@Override
	public void processPreWrite() {
		
	}

	public UInt32Part getDependencyStreamId() {
		return dependencyStreamId;
	}
	
	public Int8Part getWeight() {
		return weight;
	}
}
