package com.realtimetech.jara.http.network.frame;

import java.io.IOException;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.stream.HttpInputStream;

public class FrameOrder {
	private Frame<?> frame;
	private HttpInputStream payload;
	private byte flag;
	
	public FrameOrder(Frame<?> frame, HttpInputStream payload, byte flag) {
		this.frame = frame;
		this.payload = payload;
		this.flag = flag;
	}
	
	public byte getFlag() {
		return flag;
	}
	
	public Frame<?> getFrame() {
		return frame;
	}
	
	public HttpInputStream getPayload() {
		return payload;
	}
	
	public Frame<?> process() throws IOException, ProtocolErrorException {
		frame.readFrame(flag, payload);
		frame.processReadFrame(frame.getStream().getState());
		
		return frame;
	}
}
