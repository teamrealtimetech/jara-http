package com.realtimetech.jara.http.network.frame;

/**
 * HTTP2 프로토콜의 기본 통신 단위인 Frame의 Type에 대한 열거형이다.
 * 
 * @author ParkJeongHwan
 */
public enum FrameType {
	DATA_FRAME((byte) 0x0),
	HEADER_FRAME((byte) 0x1),
	PRIORITY_FRAME((byte) 0x2),
	RST_STREAM_FRAME((byte) 0x3),
	SETTING_FRAME((byte) 0x4),
	PUSH_PROMISE_FRAME((byte) 0x5),
	PING_FRAME((byte) 0x6),
	GOAWAY_FRAME((byte) 0x7),
	WINDOW_UPDATE_FRAME((byte) 0x8),
	CONTINUATION((byte) 0x9);

	private byte type;

	FrameType(byte type) {
		this.type = type;
	}

	public byte getType() {
		return type;
	}

	public static FrameType tryParse(byte type) {
		for (FrameType frameType : FrameType.values()) {
			if (frameType.getType() == type) {
				return frameType;
			}
		}

		return null;
	}
}