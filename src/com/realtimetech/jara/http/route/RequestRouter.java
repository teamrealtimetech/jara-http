package com.realtimetech.jara.http.route;

import com.realtimetech.jara.http.cache.CacheStorage.CacheValueGetter;
import com.realtimetech.jara.http.header.type.MethodType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.response.HttpResponse;

/**
 * 발생한 Request에 대한 Listener Class입니다.
 * 
 * @author ParkJeongHwan
 */
public abstract class RequestRouter {
	/**
	 * Path에 대한 Sub Url를 구하고 Cache Data를 관리하는 CacheValueGetter의 구현체입니다.
	 * 
	 * @author ParkJeongHwan
	 */
	public class CacheSubUrlGetter extends CacheValueGetter<String> {
		private HttpRequest httpRequest;

		public CacheSubUrlGetter(HttpRequest httpRequest) {
			this.httpRequest = httpRequest;
		}

		@Override
		public String getValue() {
			StringBuilder stringBuilder = new StringBuilder();

			String[] requestUrl = httpRequest.getPath().split("/");
			String[] routeUrl = requestUrl().split("/");

			if (routeUrl.length != 0 && requestUrl.length != 0) {
				if (routeUrl.length <= requestUrl.length) {
					int routeIndex = 0;

					for (int index = 0; index < requestUrl.length; index++) {
						boolean wildFlag = routeIndex == routeUrl.length - 1 && routeUrl[routeIndex].equals("*");
						boolean normalFlag = routeUrl.length > routeIndex && requestUrl[index].equals(routeUrl[routeIndex]);

						if (wildFlag || normalFlag) {
							if (routeIndex < routeUrl.length - 1) {
								routeIndex++;
							} else {
								if (wildFlag) {
									stringBuilder.append("/");
									stringBuilder.append(requestUrl[index]);
								}
							}
						} else {
							break;
						}
					}
				}
			}

			return stringBuilder.toString();
		}

		@Override
		public boolean isRefresh(String path) {
			return false;
		}

	}

	/**
	 * 이 Router에 대한 우선순위를 결정하는 필드이다.
	 */
	public Priority workPriority;

	/**
	 * Router에 대한 생성자이다.
	 * 
	 * @param priority 우선순위에 대한 인자이다.
	 */
	public RequestRouter(Priority priority) {
		this.workPriority = priority;
	}

	/**
	 * Response할 Request의 Url을 지정하는 메서드이다, 와일드 캐릭터인 *를 지원한다.
	 * 
	 * @return 응답의 대상이 되는 Url을 반환한다.
	 */
	public abstract String requestUrl();

	/**
	 * Response할 Request의 Method Type을 지정하는 메서드이다.
	 * 
	 * @return 응답의 대상이 되는 Method Type을 반환한다.
	 */
	public abstract MethodType requestMethod();

	/**
	 * Response할 Request의 Mime Type을 지정하는 메서드이다.
	 * 
	 * @return 응답의 대상이 되는 Mime Type을 반환한다.
	 */
	public abstract MimeType requestMimeType();

	/**
	 * Request에대한 Response을 생성하는 Process 메서드이다.
	 * 
	 * @param httpRequest  HTTP Request 인자이다.
	 * @param httpResponse HTTP Response 인자이다, 이 인자에 응답을 작성한다.
	 * 
	 * @return 응답 결과을 반환한다. true일경우 응답 성공, false일경우 응답 실패이다.
	 */
	public abstract boolean onProcess(HttpRequest httpRequest, HttpResponse httpResponse);

	/**
	 * Request에 대한 Sub Url을 가져오는 메서드이다.
	 * 
	 * @param httpRequest HTTP Request 인자이다.
	 * 
	 * @return Sub Url을 반환한다.
	 */
	public String getSubUrl(HttpRequest httpRequest) {
		return httpRequest.getStream().getConnection().getHttpServer().getCacheStorage().<String>getCachedData(httpRequest, this, "sub_urls", httpRequest.getPath(), new CacheSubUrlGetter(httpRequest));
	}

	public Priority getWorkPriority() {
		return workPriority;
	}
}
