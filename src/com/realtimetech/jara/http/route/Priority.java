package com.realtimetech.jara.http.route;

public enum Priority {
	SUPER(Integer.MAX_VALUE), HIGH(3), MIDDLE(2), LOW(1);

	private int level;

	Priority(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}
}
