package com.realtimetech.jara.http.connection.stream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * HTTP2를 지원하는 OutputStream을 구현한 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class HttpOutputStream extends OutputStream {
	/**
	 * 출격 대상이 되는 OutputStream 필드이다.
	 */
	private OutputStream outputStream;

	/**
	 * 단순 Byte Array로 출력하는 생성자이다.
	 */
	public HttpOutputStream() {
		this(new ByteArrayOutputStream());
	}

	/**
	 * 특정 OutputStream에 출력하는 생성자이다.
	 * 
	 * @param outputStream 출력 대상이 되는 OutputStream 인자이다.
	 */
	public HttpOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	/**
	 * 특정 Byte Array의 offset과 length 이용해서 출력하는 메서드이다.
	 */
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		if (b.length != 0) {
			this.outputStream.write(b, off, len);
		}
	}

	/**
	 * 특정 Byte Array를 출력하는 메서드이다.
	 */
	@Override
	public void write(byte[] b) throws IOException {
		this.outputStream.write(b);
	}

	/**
	 * 특정 Byte를 출력하는 메서드이다.
	 */
	@Override
	public void write(int b) throws IOException {
		this.outputStream.write(b);
	}

	/**
	 * 특정 Byte를 출력하는 메서드이다.
	 * 
	 * @param value 출력할 Data 인자이다.
	 * 
	 * @throws IOException 출력중 오류에 해당하는 Exception이다.
	 */
	public void writeByte(byte value) throws IOException {
		this.outputStream.write(value);
	}

	/**
	 * 16Bit 정수를 출력하는 메서드이다.
	 * 
	 * @param value 출력할 Data 인자이다.
	 * 
	 * @throws IOException 출력중 오류에 해당하는 Exception이다.
	 */
	public void write16BitInteger(int value) throws IOException {
		this.outputStream.write((value & 0x0000FF00) >> 8);
		this.outputStream.write((value & 0x000000FF) >> 0);
	}

	/**
	 * 24Bit 정수를 출력하는 메서드이다.
	 * 
	 * @param value 출력할 Data 인자이다.
	 * 
	 * @throws IOException 출력중 오류에 해당하는 Exception이다.
	 */
	public void write24BitInteger(int value) throws IOException {
		this.outputStream.write((value & 0x00FF0000) >> 16);
		this.outputStream.write((value & 0x0000FF00) >> 8);
		this.outputStream.write((value & 0x000000FF) >> 0);
	}

	/**
	 * 부호있는 32Bit 정수를 출력하는 메서드이다.
	 * 
	 * @param value 출력할 Data 인자이다.
	 * 
	 * @throws IOException 출력중 오류에 해당하는 Exception이다.
	 */
	public void writeSigned32BitInteger(int value) throws IOException {
		this.outputStream.write((value & 0xFF000000) >> 32);
		this.outputStream.write((value & 0x00FF0000) >> 16);
		this.outputStream.write((value & 0x0000FF00) >> 8);
		this.outputStream.write((value & 0x000000FF) >> 0);
	}

	/**
	 * 부호없는 32Bit 정수를 출력하는 메서드이다.
	 * 
	 * @param value 출력할 Data 인자이다.
	 * 
	 * @throws IOException 출력중 오류에 해당하는 Exception이다.
	 */
	public void writeUnsigned32BitInteger(long value) throws IOException {
		value = 0x7FFFFFFF & value;
		this.outputStream.write((byte) ((value & 0xFF000000) >> 24));
		this.outputStream.write((byte) ((value & 0x00FF0000) >> 16));
		this.outputStream.write((byte) ((value & 0x0000FF00) >> 8));
		this.outputStream.write((byte) ((value & 0x000000FF) >> 0));
	}

	/**
	 * 출력한 결과를 Byte Array로 반환받는 메서드이다.
	 * 
	 * @return 출력 결과를 반환한다.
	 */
	public byte[] toByteArray() {
		if (outputStream instanceof ByteArrayOutputStream) {
			ByteArrayOutputStream byteArrayOutputStream = (ByteArrayOutputStream) outputStream;

			return byteArrayOutputStream.toByteArray();
		}

		return null;
	}
}
