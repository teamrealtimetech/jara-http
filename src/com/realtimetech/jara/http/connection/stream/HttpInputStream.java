package com.realtimetech.jara.http.connection.stream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * HTTP2를 지원하는 InputStream을 구현한 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class HttpInputStream extends InputStream {
	/**
	 * 소스에 해당하는 InputStream 필드이다.
	 */
	private InputStream inputStream;

	/**
	 * 마지막으로 읽은 바이트에 대한 필드이다.
	 */
	private byte lastByte;
	
	/**
	 * 전체 Length에 대한 필드이다.
	 */
	private int length;

	/**
	 * InputStream의 소스로 Byte Array를 사용하는 생성자이다.
	 * 
	 * @param bytes 소스로 해당하는 Byte Array이다.
	 */
	public HttpInputStream(byte[] bytes) {
		this(new ByteArrayInputStream(bytes));
		this.length = bytes.length;
	}

	/**
	 * InputStream의 소스로 InputStream을 사용하는 생성자이다.
	 * 
	 * @param inputStream 소스로 사용하는 InputStream이다.
	 */
	public HttpInputStream(InputStream inputStream) {
		this.lastByte = 0;

		this.inputStream = inputStream;
	}

	/**
	 * Byte Array를 읽는 메서드이며 Blocking 메서드이다.
	 */
	@Override
	public int read(byte[] bytes, int offset, int size) throws IOException {
		long totalRead = 0;

		do {
			int read = inputStream.read(bytes, (int) totalRead + offset, size - (int) totalRead);
			if (read > 0) {
				totalRead += read;
			}else {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					throw new IOException("Interrupted read");
				}
			}
		} while (totalRead < size);

		lastByte = bytes[offset + size - 1];

		return size;
	}

	/**
	 * Byte Array를 읽는 메서드이며 Blocking 메서드이다.
	 */
	@Override
	public int read(byte[] bytes) throws IOException {
		return read(bytes, 0, bytes.length);
	}

	/**
	 * Byte를 읽는 메서드이며 Blocking 메서드이다.
	 */
	@Override
	public int read() throws IOException {
		int read = this.inputStream.read();
		lastByte = (byte) read;

		return read;
	}

	/**
	 * 마지막으로 읽었던 Byte를 반환하는 메서드이다.
	 * 
	 * @return Byte를 반환한다.
	 */
	public byte getLastByte() {
		return lastByte;
	}
	
	public int getLength() {
		return length;
	}

	public byte readByte() throws IOException {
		return (byte) read();
	}

	/**
	 * 특정 사이즈만큼 읽어서 Byte Array로 반환하는 메서드이다.
	 * 
	 * @param size 사이즈에 대한 인자이다.
	 * 
	 * @return 읽은 결과를 반환한다.
	 * 
	 * @throws IOException 수신중 오류에 해당하는 Exception이다.
	 */
	public byte[] readBytes(int size) throws IOException {
		if(size == 0) return new byte[0];
		byte[] bytes = new byte[size];

		read(bytes);

		return bytes;
	}

	/**
	 * 특정 사이즈만큼 읽어서 HttpInputStream으로 반환하는 메서드이다.
	 * 
	 * @param size 사이즈에 대한 인자이다.
	 * 
	 * @return 읽은 결과를 반환한다.
	 * 
	 * @throws IOException 수신중 오류에 해당하는 Exception이다.
	 */
	public HttpInputStream readStream(int size) throws IOException {
		return new HttpInputStream(readBytes(size));
	}

	/**
	 * 16Bit 정수를 읽어서 반환하는 메서드이다.
	 * 
	 * @return 읽은 결과를 반환한다.
	 * 
	 * @throws IOException 수신중 오류에 해당하는 Exception이다.
	 */
	public int read16BitInteger() throws IOException {
		int data;

		data = read() << 8;
		data += read();

		return data;
	}

	/**
	 * 24Bit 정수를 읽어서 반환하는 메서드이다.
	 * 
	 * @return 읽은 결과를 반환한다.
	 * 
	 * @throws IOException 수신중 오류에 해당하는 Exception이다.
	 */
	public int read24BitInteger() throws IOException {
		int data;

		data = read() << 16;
		data = read() << 8;
		data += read();

		return data;
	}


	/**
	 * 32Bit 정수를 읽어서 반환하는 메서드이다.
	 * 
	 * @return 읽은 결과를 반환한다.
	 * 
	 * @throws IOException 수신중 오류에 해당하는 Exception이다.
	 */
	public int readSigned32BitInteger() throws IOException {
		int data;

		data = read() << 24;
		data += read() << 16;
		data += read() << 8;
		data += read();

		return data;
	}

	/**
	 * 부호없는 32Bit 정수를 읽어서 반환하는 메서드이다.
	 * 
	 * @return 읽은 결과를 반환한다.
	 * 
	 * @throws IOException 수신중 오류에 해당하는 Exception이다.
	 */
	public long readUnsigned32BitInteger() throws IOException {
		long data;

		data = read() << 24;
		data += read() << 16;
		data += read() << 8;
		data += read();

		return data;
	}
}
