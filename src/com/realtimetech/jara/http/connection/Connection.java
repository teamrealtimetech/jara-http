package com.realtimetech.jara.http.connection;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.connection.exception.ProtocolErrorType;
import com.realtimetech.jara.http.connection.stream.HttpInputStream;
import com.realtimetech.jara.http.connection.stream.HttpOutputStream;
import com.realtimetech.jara.http.header.hpack.Hpack;
import com.realtimetech.jara.http.network.frame.Frame;
import com.realtimetech.jara.http.network.frame.FrameOrder;
import com.realtimetech.jara.http.network.frame.FrameType;
import com.realtimetech.jara.http.network.frame.impl.ContinuationFrame;
import com.realtimetech.jara.http.network.frame.impl.DataFrame;
import com.realtimetech.jara.http.network.frame.impl.GoAwayFrame;
import com.realtimetech.jara.http.network.frame.impl.HeaderFrame;
import com.realtimetech.jara.http.network.frame.impl.PriorityFrame;
import com.realtimetech.jara.http.network.frame.impl.PushPromiseFrame;
import com.realtimetech.jara.http.network.frame.impl.RstStreamFrame;
import com.realtimetech.jara.http.network.frame.impl.SettingFrame;
import com.realtimetech.jara.http.network.frame.impl.WindowUpdateFrame;
import com.realtimetech.jara.http.network.frame.impl.structures.ContinuationStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.DataStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.GoAwayStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.HeaderStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.PriorityStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.PushPromiseStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.RstStreamStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.SettingStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.WindowUpdateStructure;
import com.realtimetech.jara.http.network.frame.impl.structures.setting.Settings;
import com.realtimetech.jara.http.network.stream.Stream;
import com.realtimetech.jara.http.network.stream.StreamThread;
import com.realtimetech.jara.http.network.structure.Structure;

/**
 * HTTP2 프로토콜의 Connection의 구현체이다.
 * 
 * @author ParkJeongHwan
 */
public class Connection {
	/**
	 * Connection의 0번에 해당하는 Stream 필드이다, Connection에 기본 Frame들을 주고 받는 용도의 Stream이다.
	 */
	private Stream connectionStream;

	/**
	 * Connection이 발생한 HttpServer 필드이다.
	 */
	private HttpServer httpServer;

	/**
	 * Client의 Settings에 대한 필드이다.
	 */
	private Settings remoteSettings;

	/**
	 * Server의 Settings에 대한 필드이다.
	 */
	private Settings localSettings;

	/**
	 * Stream의 고유 Id부여를 위한 필드이다. (항상 짝수로 유지되어야 한다.)
	 */
	private long globalStreamId;

	/**
	 * Connection의 실제 TCP Socket이다.
	 */
	private Socket socket;

	/**
	 * Connection이 관리하고있는 Stream의 Map이다, Key는 Stream의 Id를 이용하며 Value는 Stream이다.
	 */
	private HashMap<Long, Stream> streams;

	/**
	 * Socket의 InputStream을 HttpInputStream에 Wrapping한 필드이다.
	 */
	private HttpInputStream httpInputStream;

	/**
	 * Socket의 OutputStream을 HttpOutputStream에 Wrapping한 필드이다.
	 */
	private HttpOutputStream httpOutputStream;

	/**
	 * Connection에 각각의 EndPoint과 항상 공유되는 Header Table에 대한 Hpack 필드이다.
	 */
	private Hpack receiveHpack;
	private Hpack sendHpack;

	/**
	 * 전송할 Frame에 대한 Queue 필드이다.
	 */
	private LinkedBlockingQueue<Frame<?>> sendOrders;

	/**
	 * Frame 수신을 위한 Thread 필드이다.
	 */
	private Thread receiveThread;

	/**
	 * Frame 발신을 위한 Thread 필드이다.
	 */
	private Thread sendThread;

	/**
	 * Stream들을 처리하는 Thread 리스트 필드이다.
	 */
	private List<StreamThread> streamThreads;

	/**
	 * 처리가 필요한 Stream들의 Queue 필드이다.
	 */
	private LinkedBlockingQueue<Stream> streamQueue;

	/**
	 * Connection의 생성자이다.
	 * 
	 * @param httpServer Connection이 이뤄진 HttpServer 인자이다.
	 * @param socket     Connection이 이뤄진 Socket 인자이다.
	 * 
	 * @throws IOException Connection 수립중 발생하는 에러에 대한 Exception이다.
	 */
	public Connection(HttpServer httpServer, Socket socket) throws IOException {
		this.remoteSettings = new Settings();
		this.localSettings = new Settings();
		this.globalStreamId = 0;
		this.httpServer = httpServer;
		this.socket = socket;
		this.streams = new HashMap<Long, Stream>();
		this.receiveHpack = new Hpack();
		this.sendHpack = new Hpack();

		this.sendOrders = new LinkedBlockingQueue<Frame<?>>();

		this.streamThreads = new LinkedList<StreamThread>();
		this.streamQueue = new LinkedBlockingQueue<Stream>();

		try {
			this.connectionStream = createStream();
		} catch (ProtocolErrorException e1) {
			e1.printStackTrace();
		}
		this.streams.put((long) 0, this.connectionStream);

		this.httpInputStream = new HttpInputStream(socket.getInputStream());
		this.httpOutputStream = new HttpOutputStream(socket.getOutputStream());

		this.receiveThread = new Thread(() -> {
			try {
				processReceive();
			} catch (ProtocolErrorException e) {
				sendProtocolError(e);
			} catch (IOException e) {
				this.deactive();
			}
		});

		this.sendThread = new Thread(() -> {
			try {
				processSend();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ProtocolErrorException e) {
				sendProtocolError(e);
			} catch (InterruptedException e) {

			}
		});
	}

	/**
	 * Connection의 전송용 Hpack을 반환하는 메서드이다.
	 * 
	 * @return Hpack을 반환한다.
	 */
	public Hpack getSendHpack() {
		return sendHpack;
	}

	/**
	 * Connection의 수신용 Hpack을 반환하는 메서드이다.
	 * 
	 * @return Hpack을 반환한다.
	 */
	public Hpack getReceiveHpack() {
		return receiveHpack;
	}

	/**
	 * 해당하는 HttpServer 객체를 반환하는 메서드이다.
	 * 
	 * @return HttpServer를 반환한다.
	 */
	public HttpServer getHttpServer() {
		return httpServer;
	}

	/**
	 * StreamThread 리스트를 반환하는 메서드이다.
	 * 
	 * @return StreamThread 리스트를 반환한다.
	 */
	public List<StreamThread> getStreamThreads() {
		return streamThreads;
	}

	/**
	 * 처리가 필요한 Stream을 가진 Queue를 반환하는 메서드이다.
	 * 
	 * @return Stream Queue를 반환한다.
	 */
	public LinkedBlockingQueue<Stream> getStreamQueue() {
		return streamQueue;
	}

	/**
	 * Server의 셋팅을 새로고침하여 Client에게 전송하는 메서드이다.
	 */
	public void refreshSetting() {
		this.sendFrame(new SettingFrame(this.connectionStream).setSettings(localSettings));
	}

	/**
	 * Remote Settings를 반환하는 메서드이다.
	 * 
	 * @return Remote Settings를 반환한다.
	 */
	public Settings getRemoteSettings() {
		return remoteSettings;
	}

	/**
	 * Local Settings를 반환하는 메서드이다.
	 * 
	 * @return Local Settings를 반환한다.
	 */
	public Settings getLocalSettings() {
		return localSettings;
	}

	/**
	 * 발생한 Protocol Exception을 Client에게 전송하는 메서드이다.
	 * 
	 * @param e 발생한 Exception에 대한 인자이다.
	 */
	public void sendProtocolError(ProtocolErrorException e) {
		if (e.getProtocolErrorType() == ProtocolErrorType.SERVER_RULE_ERROR) {
			e.printStackTrace();
		} else {
			if (e.getStream() == this.connectionStream) {
				GoAwayFrame frame = new GoAwayFrame(e.getStream());

				frame.setErrorType(e.getProtocolErrorType());
				frame.setDebugData(e.getMessage().getBytes());

				sendFrame(frame);
			} else {
				RstStreamFrame frame = new RstStreamFrame(e.getStream());

				frame.setErrorType(e.getProtocolErrorType());

				sendFrame(frame);
			}
		}
	}

	/**
	 * 서버측에서 새로운 Stream을 생성하는 메서드이다.
	 * 
	 * @return 생성된 새로운 Stream이다.
	 * 
	 * @throws ProtocolErrorException 생성된 새로운 Stream Id가 HTTP2 프로토콜에 위배될 시 발생하는 Exception이다.
	 */
	public Stream createStream() throws ProtocolErrorException {
		// Stream 생성단계에서 새로운 Stream Id를 발급한다, 이때 항상 짝수로 유지되어야하는 프로토콜 규칙을 준수하여 2를 더하여 생성해준다.
		long newStreamId = globalStreamId;

		globalStreamId += 2;
		if (newStreamId != 0 && newStreamId % 2 != 0) {
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, this.connectionStream, "Stream Id of Server Stream is odd.");
		}

		return createNewStream(newStreamId);
	}

	/**
	 * 클라이언트측에서 생성된 Stream을 생성하는 메서드이다.
	 * 
	 * @param streamId 생성할 Stream의 Id에 대한 인자이다.
	 * 
	 * @return 클라이언트에서 생성된 Stream이다.
	 * 
	 * @throws ProtocolErrorException HTTP2 프로토콜에 위배될 시 발생하는 Exception이다.
	 */
	public Stream createStream(long streamId) throws ProtocolErrorException {
		if (streamId % 2 == 0) {
			throw new ProtocolErrorException(ProtocolErrorType.PROTOCOL_ERROR, this.connectionStream, "Stream Id of Client Stream is even.");
		}

		return createNewStream(streamId);
	}

	/**
	 * 실제로 Stream을 생성하는 메서드이다.
	 * 
	 * @param streamId 생성할 Stream의 Id에 대한 인자이다.
	 * 
	 * @return 생성한 Stream을 반환한다.
	 */
	private Stream createNewStream(long streamId) {
		Stream stream = new Stream(this, streamId);
		this.streams.put(streamId, stream);

		refreshStreamThreads();

		return stream;
	}

	/**
	 * 처리량에 따라서 StreamThread를 생성하는 메서드이다.
	 */
	public void refreshStreamThreads() {
		if (((float) (streamQueue.size() + 1) / 2.0f > streamThreads.size()) && httpServer.getMaxStreamThreadPerConnection() > streamThreads.size()) {
			createStreamThread();
		}
	}

	/**
	 * 실제로 StreamThread를 생성하는 메서드이다.
	 * 
	 * @return 생성한 StreamThread를 반환한다.
	 */
	public StreamThread createStreamThread() {
		StreamThread streamThread = new StreamThread(this);

		streamThread.active();

		synchronized (streamThreads) {
			streamThreads.add(streamThread);
		}

		return streamThread;
	}

	/**
	 * 특정 StreamThread를 제거하고 비활성화하는 메서드이다.
	 * 
	 * @param streamThread 제거할 StreamThread 인자이다.
	 */
	public void removeStreamThread(StreamThread streamThread) {
		streamThread.deactive();

		synchronized (streamThreads) {
			streamThreads.remove(streamThread);
		}
	}

	/**
	 * 기존에 생성되었던 Stream을 가져오는 메서드이다.
	 * 
	 * @param streamId Stream을 식별할 수 있는 Id 인자이다.
	 * 
	 * @return 생성되었던 Stream이다.
	 * 
	 * @throws ProtocolErrorException Stream 처리중 Protocol 규칙에 어긋날 경우 발생하는 Exception이다.
	 */
	public Stream getStream(long streamId) throws ProtocolErrorException {
		Stream stream = this.streams.get(streamId);

		if (stream != null) {
			return stream;
		} else {
			return createStream(streamId);
		}
	}

	/**
	 * Frame을 수신하는 메서드이다.
	 * 
	 * @return 수신한 Frame을 반환한다.
	 * 
	 * @throws IOException            수신중 발생하는 Exception에 해당한다.
	 * @throws ProtocolErrorException 수신 후 검사한 Protocol 오류에 대한 Exception에 해당한다.
	 */
	public FrameOrder receiveFrame() throws IOException, ProtocolErrorException {
		int length = httpInputStream.read24BitInteger();
		FrameType frameType = FrameType.tryParse(httpInputStream.readByte());
		byte frameFlag = httpInputStream.readByte();
		long streamId = httpInputStream.readUnsigned32BitInteger();
		if (length < 0)
			return null;
		HttpInputStream payload = httpInputStream.readStream(length);

		Frame<?> frame = null;

		Stream stream = getStream(streamId);
		Structure structure = stream.getCacheStructure(frameType);

		switch (frameType) {
		case DATA_FRAME:
			frame = new DataFrame(stream, (DataStructure) structure);
			break;
		case SETTING_FRAME:
			frame = new SettingFrame(stream, (SettingStructure) structure);
			break;
		case HEADER_FRAME:
			frame = new HeaderFrame(stream, (HeaderStructure) structure);
			break;
		case PUSH_PROMISE_FRAME:
			frame = new PushPromiseFrame(stream, (PushPromiseStructure) structure);
			break;
		case PRIORITY_FRAME:
			frame = new PriorityFrame(stream, (PriorityStructure) structure);
			break;
		case RST_STREAM_FRAME:
			frame = new RstStreamFrame(stream, (RstStreamStructure) structure);
			break;
		case WINDOW_UPDATE_FRAME:
			frame = new WindowUpdateFrame(stream, (WindowUpdateStructure) structure);
			break;
		case GOAWAY_FRAME:
			frame = new GoAwayFrame(stream, (GoAwayStructure) structure);
			break;
		case CONTINUATION:
			frame = new ContinuationFrame(stream, (ContinuationStructure) structure);
			break;
		default:
			break;
		}

		if (frame != null) {
			return frame.preOrderReadFrame(frameFlag, payload);
		} else {
		}

		return null;
	}

	/**
	 * Connection을 활성화 한다.
	 */
	public void active() {
		this.receiveThread.start();
		this.sendThread.start();
	}

	/**
	 * Connection을 비활성화 한다.
	 */
	public void deactive() {
		for (StreamThread streamThread : streamThreads) {
			streamThread.deactive();
		}
		this.receiveThread.interrupt();
		this.sendThread.interrupt();

		try {
			this.socket.close();
		} catch (IOException e) {
		}
	}

	/**
	 * Frame 수신을 위한 처리 메서드이다.
	 * 
	 * @throws IOException            수신중 발생하는 Exception에 해당한다.
	 * @throws ProtocolErrorException 수신 후 검사한 Protocol 오류에 대한 Exception에 해당한다.
	 */
	public void processReceive() throws ProtocolErrorException, IOException {
		while (true) {
			FrameOrder frameOrder = receiveFrame();
			if (frameOrder != null) {
				frameOrder.getFrame().getStream().getFrameOrders().offer(frameOrder);
				this.streamQueue.offer(frameOrder.getFrame().getStream());
			}
		}
	}

	/**
	 * Frame 발신을 위한 처리 메서드이다.
	 * 
	 * @throws IOException            발신중 발생하는 Exception에 해당한다.
	 * @throws ProtocolErrorException 발신 전 검사한 Protocol 오류에 대한 Exception에 해당한다.
	 * @throws InterruptedException   Connection 비활성화 시 발생하는 Exception에 해당한다.
	 */
	public void processSend() throws ProtocolErrorException, IOException, InterruptedException {
		this.sendFrame(new SettingFrame(this.connectionStream).setSettings(new Settings(new HashMap<>())));

		while (true) {
			Frame<?> take = sendOrders.take();

			take.writeFrame(httpOutputStream);
		}
	}

	/**
	 * Frame을 발신하는 메서드이다.
	 * 
	 * @param frame 발신할 Frame에 대한 인자이다.
	 */
	public void sendFrame(Frame<?> frame) {
		try {
			frame.processWriteFrame(frame.getStream().getState());
		} catch (ProtocolErrorException e) {
			sendProtocolError(e);
		}
		this.sendOrders.offer(frame);
	}

	/**
	 * Connection의 Socket을 반환하는 메서드이다.
	 * 
	 * @return Socket을 반환한다.
	 */
	public Socket getSocket() {
		return socket;
	}

	/**
	 * Connection의 HttpInputStream을 반환하는 메서드이다.
	 * 
	 * @return HttpInputStream을 반환한다.
	 */
	public HttpInputStream getHttpInputStream() {
		return httpInputStream;
	}

	/**
	 * Connection의 HttpOutputStream을 반환하는 메서드이다.
	 * 
	 * @return HttpOutputStream을 반환한다.
	 */
	public HttpOutputStream getHttpOutputStream() {
		return httpOutputStream;
	}

	/**
	 * Connection의 Stream을 반환하는 메서드이다.
	 * 
	 * @return Connection의 Stream을 반환한다.
	 */
	public Stream getConnectionStream() {
		return connectionStream;
	}
}
