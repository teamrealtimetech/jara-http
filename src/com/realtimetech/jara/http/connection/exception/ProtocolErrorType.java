package com.realtimetech.jara.http.connection.exception;

/**
 * HTTP2 Protocol Error에 대한 Type 열거형이다.
 * 
 * @author ParkJeongHwan
 */
public enum ProtocolErrorType {
	NO_ERROR((byte) 0x0),
	PROTOCOL_ERROR((byte) 0x1),
	INTERNAL_ERROR((byte) 0x2),
	FLOW_CONTROL_ERROR((byte) 0x3),
	SETTINGS_TIMEOUT((byte) 0x4),
	STREAM_CLOSED((byte) 0x5),
	FRAME_SIZE_ERROR((byte) 0x6),
	REFUSED_STREAM((byte) 0x7),
	CANCEL((byte) 0x8),
	COMPRESSION_ERROR((byte) 0x9),
	CONNECT_ERROR((byte) 0xa),
	ENHANCE_YOUR_CALM((byte) 0xb),
	INADEQUATE_SECURITY((byte) 0xc),
	HTTP_1_1_REQUIRED((byte) 0xd),
	SERVER_RULE_ERROR((byte) 0x0);

	private byte type;

	ProtocolErrorType(byte type) {
		this.type = type;
	}

	public byte getType() {
		return type;
	}

	public static ProtocolErrorType tryParse(byte type) {
		for (ProtocolErrorType protocolErrorType : ProtocolErrorType.values()) {
			if (protocolErrorType.getType() == type) {
				return protocolErrorType;
			}
		}

		return null;
	}
}