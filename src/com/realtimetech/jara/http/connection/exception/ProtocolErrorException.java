package com.realtimetech.jara.http.connection.exception;

import com.realtimetech.jara.http.network.stream.Stream;

/**
 * HTTP2 Protocol 규칙에 위배되는 상태의 Exception이다.
 * 
 * @author ParkJeongHwan
 */
public class ProtocolErrorException extends Exception {
	private static final long serialVersionUID = -6165941734151748058L;

	/**
	 * Exception의 Type에 해당하는 필드이다.
	 */
	private ProtocolErrorType protocolErrorType;
	
	/**
	 * Exception이 발생한 Stream에 해당하는 필드이다.
	 */
	private Stream stream;
	
	/**
	 * 상세한 Exception Message 필드이다.
	 */
	private String message;

	public ProtocolErrorException(ProtocolErrorType protocolErrorType, Stream stream, String message) {
		this.protocolErrorType = protocolErrorType;
		this.stream = stream;
		this.message = message;
	}

	public ProtocolErrorType getProtocolErrorType() {
		return protocolErrorType;
	}

	public Stream getStream() {
		return stream;
	}

	@Override
	public String getMessage() {
		return "HTTP2 protocol '" + protocolErrorType.toString() + "' violated(" + message + ").";
	}
}
