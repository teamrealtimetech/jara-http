package com.realtimetech.jara.http.header;

import java.util.HashMap;

import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * Header의 Type은 식별가능하나 단순 Header로 이용할 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class StaticHeader extends TypeHeader {
	/**
	 * Header의 값에 해당하는 필드이다.
	 */
	private String value;

	/**
	 * Header의 Type에 해당하는 필드이다.
	 */
	private HeaderType type;

	public StaticHeader(HeaderType type, String value) {
		this(type, value, true);
	}

	public StaticHeader(HeaderType type, String value, boolean indexing) {
		super(indexing, true);

		this.type = type;
		this.value = value;
	}

	@Override
	public HeaderType getType() {
		return type;
	}

	@Override
	public void parse(String value, HashMap<String, String> options) {
		this.value = value;
	}

	@Override
	public void buildValue(HeaderValueGenerator headerValueGenerator) {
		headerValueGenerator.setValue(value);
	}
}
