package com.realtimetech.jara.http.header;

/**
 * HTTP2 Header에 대한 추상화 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public abstract class Header {
	/**
	 * Header 이름를 반환하는 메서드이다.
	 * 
	 * @return Header 이름을 반환한다.
	 */
	public abstract String getName();

	/**
	 * Header 값을 반환하는 메서드이다.
	 * 
	 * @return Header 값을 반환한다.
	 */
	public abstract String getValue();

	/**
	 * 해당 Header가 Indexing을 할 Header인지 반환하는 메서드이다.
	 * 
	 * @return Indexing 여부를 반환한다.
	 */
	public abstract boolean isIndexing();

	/**
	 * 해당 Header를 Huffman Encoding을 할 Header인지 반환하는 메서드이다.
	 * 
	 * @return Huffman Encoding 여부를 반환한다.
	 */
	public abstract boolean isHuffman();
	
	@Override
	public String toString() {
		return getName() + ": " + getValue(); 
	}
}
