package com.realtimetech.jara.http.header;

import java.util.HashMap;

import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.header.type.MethodType;

/**
 * Method에 대한 Header의 구현체이다.
 * 
 * @author ParkJeongHwan
 */
public class MethodHeader extends TypeHeader {
	/**
	 * 메서드 Type의 해당하는 필드이다.
	 */
	private MethodType methodType;

	public MethodHeader(MethodType methodType) {
		this(methodType, true, true);
	}

	public MethodHeader(MethodType methodType, boolean huffman) {
		this(methodType, huffman, true);
	}

	public MethodHeader(MethodType methodType, boolean huffman, boolean indexing) {
		super(huffman, indexing);

		this.methodType = methodType;
	}

	/**
	 * 메서드 Type을 반환하는 메서드이다.
	 * 
	 * @return 메서드 Type을 반환한다.
	 */
	public MethodType getMethodType() {
		return methodType;
	}

	@Override
	public HeaderType getType() {
		return HeaderType.METHOD;
	}

	@Override
	public void parse(String value, HashMap<String, String> options) {
		this.methodType = MethodType.tryParse(value);
	}

	@Override
	public void buildValue(HeaderValueGenerator headerValueGenerator) {
		headerValueGenerator.setValue(methodType.getValue());
	}
}
