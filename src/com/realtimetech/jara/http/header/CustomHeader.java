package com.realtimetech.jara.http.header;

/**
 * 알 수 없는 Header에 대한 구현체이다.
 * 
 * @author ParkJeongHwan
 */
public class CustomHeader extends Header {
	private String name;
	private String value;
	
	private boolean indexing;
	
	public CustomHeader(String name, String value) {
		this(name, value, false);
	}
	
	public CustomHeader(String name, String value, boolean indexing) {
		this.name = name;
		this.value = value;
		this.indexing = indexing;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public boolean isIndexing() {
		return indexing;
	}

	@Override
	public boolean isHuffman() {
		return false;
	}
}
