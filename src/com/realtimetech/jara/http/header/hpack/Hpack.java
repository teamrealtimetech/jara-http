package com.realtimetech.jara.http.header.hpack;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import com.realtimetech.jara.http.connection.exception.ProtocolErrorException;
import com.realtimetech.jara.http.header.Header;
import com.realtimetech.jara.http.header.StaticHeader;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.collection.DynamicHeaderTable;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.hpack.huffman.Huffman;
import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * HTTP2 표준 구현을 위한 HPACK 구현 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class Hpack {
	/**
	 * Header Read/Write시의 비트 연산을 위한 ByteBuffer 클래스이다. (개편이 요구됨)
	 * 
	 * @author ParkJeongHwan
	 */
	public static class ByteBuf {
		private byte[] bytes;
		private int offset;

		public ByteBuf(byte[] bytes) {
			this.bytes = bytes;
			this.offset = -1;
		}

		public byte getByte() {
			return bytes[offset];
		}

		public byte getByteAndIncrease() {
			return bytes[offset++];
		}

		public byte nextByte() {
			offset += 1;
			return bytes[offset];
		}

		public byte[] getBytes() {
			return bytes;
		}

		public int getOffset() {
			return offset;
		}

		public int increaseOffset() {
			offset += 1;
			return offset;
		}

		public int getOffsetAndIncrease() {
			return offset++;
		}
	}

	/**
	 * HPACK 구현시 EndPoint간의 Table 공유가 요구됨에 따라서 공유될 Table의 필드이다.
	 */
	private DynamicHeaderTable headerTable;

	/**
	 * HPACK의 생성자이다, 최초 생성시 HeaderTable의 Static Table을 클론하여 HeaderTable을 구성한다.
	 */
	public Hpack() {
		this.headerTable = new DynamicHeaderTable();
	}

	/**
	 * 현재의 HeaderTable을 반환하는 매서드이다.
	 * 
	 * @return HeaderTable을 반환한다.
	 */
	public DynamicHeaderTable getHeaderTable() {
		return headerTable;
	}

	/**
	 * Header의 Type에 해당되는 Enum이다.
	 * 
	 * @author ParkJeongHwan
	 */
	public static enum HeaderRequestType {
		INDEXED_HEADER_FIELD_REPRESENTATION((byte) 0x80),
		LITERAL_HEADER_FIELD_WITH_INCREMENTAL_INDEXING((byte) 0x40),
		LITERAL_HEADER_FIELD_NEVER_INDEXED((byte) 0x10),
		LITERAL_HEADER_FIELD_WITHOUT_INDEXING((byte) 0x00);

		private byte type;

		HeaderRequestType(byte type) {
			this.type = type;
		}

		public byte getType() {
			return type;
		}
	}

	/**
	 * Encoding된 Byte Array을 HPACK Decoding을 한뒤 HeaderSet을 반환하는 메서드이다.
	 * 
	 * @param data Encoding된 Byte Array인자이다.
	 * 
	 * @return Decoding된 HeaderSet을 반환한다.
	 * 
	 * @throws IOException            주어진 Byte Array의 연산 과정중 발생하는 에러에 대한 Exception이다.
	 * @throws ProtocolErrorException HPACK 표준 규격을 통한 Decoding 중 위반 사항에 대한 Exception이다.
	 */
	public synchronized HeaderSet decode(byte[] data) throws IOException, ProtocolErrorException {
		ByteBuf byteBuf = new ByteBuf(data);
		HeaderSet headerSet = new HeaderSet();

		while (byteBuf.offset < byteBuf.bytes.length - 2) {
			HeaderRequestType headerRequestType = HeaderRequestType.INDEXED_HEADER_FIELD_REPRESENTATION;
			byte header = byteBuf.nextByte();
			int index = -1;

			if (header < 0) {
				headerRequestType = HeaderRequestType.INDEXED_HEADER_FIELD_REPRESENTATION;

				index = readLiteralInteger(byteBuf, 1);
			} else if ((header & 0x40) == 0x40) {
				headerRequestType = HeaderRequestType.LITERAL_HEADER_FIELD_WITH_INCREMENTAL_INDEXING;

				index = readLiteralInteger(byteBuf, 2);
			} else if ((header & 0x10) == 0x10) {
				headerRequestType = HeaderRequestType.LITERAL_HEADER_FIELD_NEVER_INDEXED;

				index = readLiteralInteger(byteBuf, 4);
			} else {
				headerRequestType = HeaderRequestType.LITERAL_HEADER_FIELD_WITHOUT_INDEXING;

				index = readLiteralInteger(byteBuf, 4);
			}

			Header headerObject = null;
			String name = null;
			String value = null;

			if (headerRequestType == HeaderRequestType.INDEXED_HEADER_FIELD_REPRESENTATION) {
				headerObject = headerTable.get(index);
			} else {
				if (index != 0) {
					value = readLiteralString(byteBuf);
				} else {
					name = readLiteralString(byteBuf);
					value = readLiteralString(byteBuf);
				}

				if (name == null) {
					Header object = headerTable.get(index);
					if (headerRequestType == HeaderRequestType.LITERAL_HEADER_FIELD_WITH_INCREMENTAL_INDEXING) {
						headerObject = headerTable.add(object.getName(), value);
					} else if (headerRequestType == HeaderRequestType.LITERAL_HEADER_FIELD_NEVER_INDEXED || headerRequestType == HeaderRequestType.LITERAL_HEADER_FIELD_WITHOUT_INDEXING) {
						if (object instanceof StaticHeader) {
							headerObject = new StaticHeader(((StaticHeader) object).getType(), object.getValue());
						} else if (object instanceof TypeHeader) {
							headerObject = ((TypeHeader) object).getType().buildHeader(value);
						} else {
							headerObject = HeaderType.tryBuild(object.getName(), value);
						}
					}
				} else {
					if (headerRequestType == HeaderRequestType.LITERAL_HEADER_FIELD_WITH_INCREMENTAL_INDEXING) {
						headerObject = headerTable.add(name, value);
					} else if (headerRequestType == HeaderRequestType.LITERAL_HEADER_FIELD_NEVER_INDEXED || headerRequestType == HeaderRequestType.LITERAL_HEADER_FIELD_WITHOUT_INDEXING) {
						headerObject = HeaderType.tryBuild(name, value);
					}
				}
			}

			headerSet.add(headerObject);
		}
		return headerSet;
	}

	/**
	 * HeaderSet을 HPACK Encoding을 한뒤 Byte Array를 반환하는 메서드이다.
	 * 
	 * @param headerSet HeaderSet 인자이다.
	 * 
	 * @return Encoding된 Byte Array를 반환한다.
	 * 
	 * @throws IOException            주어진 HeaderSet를 Byte Array화 연산 과정중 발생하는 에러에 대한 Exception이다.
	 * @throws ProtocolErrorException HPACK 표준 규격을 통한 Encoding 중 위반 사항에 대한 Exception이다.
	 */
	public synchronized byte[] encode(HeaderSet headerSet) throws IOException, ProtocolErrorException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		TreeMap<Integer, List<Header>> sortedHeaders = new TreeMap<Integer, List<Header>>();
		HashMap<Header, HeaderRequestType> sortedTypes = new HashMap<Header, HeaderRequestType>();

		for (Header header : headerSet.getHeaders()) {
			HeaderRequestType headerRequestType = null;
			int index = 0;

			if (header.isIndexing()) {
				headerRequestType = HeaderRequestType.LITERAL_HEADER_FIELD_WITH_INCREMENTAL_INDEXING;
			} else {
				headerRequestType = HeaderRequestType.LITERAL_HEADER_FIELD_WITHOUT_INDEXING;
			}

			for (int i = 1; i < headerTable.size() + 1; i++) {
				Header dynamicHeader = headerTable.get(i);
				if (dynamicHeader.getName().equals(header.getName())) {
					index = headerTable.get(dynamicHeader);

					if (dynamicHeader.getValue().equals(header.getValue())) {
						headerRequestType = HeaderRequestType.INDEXED_HEADER_FIELD_REPRESENTATION;

						break;
					}
				}

			}

			if (!sortedHeaders.containsKey(index)) {
				sortedHeaders.put(index, new LinkedList<Header>());
			}

			sortedHeaders.get(index).add(header);
			sortedTypes.put(header, headerRequestType);
		}

		for (Integer index : sortedHeaders.keySet()) {
			List<Header> list = sortedHeaders.get(index);
			for (Header header : list) {
				HeaderRequestType headerRequestType = sortedTypes.get(header);

				switch (headerRequestType) {
				case INDEXED_HEADER_FIELD_REPRESENTATION:
					writeLiteralInteger(byteArrayOutputStream, headerRequestType.getType(), index, 1);
					break;
				case LITERAL_HEADER_FIELD_WITH_INCREMENTAL_INDEXING:
					writeLiteralInteger(byteArrayOutputStream, headerRequestType.getType(), index, 2);
					headerTable.add(header);
					break;
				case LITERAL_HEADER_FIELD_NEVER_INDEXED:
					writeLiteralInteger(byteArrayOutputStream, headerRequestType.getType(), index, 4);
					break;
				case LITERAL_HEADER_FIELD_WITHOUT_INDEXING:
					writeLiteralInteger(byteArrayOutputStream, headerRequestType.getType(), index, 4);
					break;
				default:
					break;
				}

				if (headerRequestType != HeaderRequestType.INDEXED_HEADER_FIELD_REPRESENTATION) {
					if (index == 0) {
						writeLiteralString(byteArrayOutputStream, header.isHuffman(), header.getName());
					}
					writeLiteralString(byteArrayOutputStream, header.isHuffman(), header.getValue());
				}
			}
		}

		return byteArrayOutputStream.toByteArray();
	}

	/**
	 * HPACK 값 표현 규격 중 String에 해당하는 값을 쓰기 위한 메서드이다.
	 * 
	 * @param outputStream 쓰여질 Stream 인자이다.
	 * @param huffman      Huffman Encoding이 요구되는 여부를 나타내는 인자이다.
	 * @param value        실제로 쓰여질 Value에 대한 인자이다.
	 * 
	 * @throws IOException            값 작성중 발생하는 오류에 대한 Exception이다.
	 * @throws ProtocolErrorException 값 작성중 발생하는 HPACK 규격 위반에 대한 Exception이다.
	 */
	public static void writeLiteralString(OutputStream outputStream, boolean huffman, String value) throws IOException, ProtocolErrorException {
		byte flag = (byte) 0x00;
		byte[] data = value.getBytes();
		int length;

		if (huffman) {
			flag = (byte) 0x80;
			length = Huffman.getEncodedLength(data);
		} else {
			length = data.length;
		}

		writeLiteralInteger(outputStream, flag, length, 1);

		if (huffman) {
			Huffman.encode(outputStream, data);
		} else {
			outputStream.write(data);
		}
	}

	/**
	 * HPACK 값 표현 규격 중 String에 해당하는 값을 읽기 위한 메서드이다.
	 * 
	 * @param byteBuf 읽을 대상이 되는 ByteBuffer 인자이다.
	 * 
	 * @return 읽어낸 String을 반환한다.
	 * 
	 * @throws IOException            값을 읽는 도중 발생하는 오류에 대한 Exception이다.
	 * @throws ProtocolErrorException 값을 읽는 도중 발생하는 HPACK 규격 위반에 대한 Exception이다.
	 */
	public static String readLiteralString(ByteBuf byteBuf) throws IOException, ProtocolErrorException {
		boolean huffmanFlag = ((byteBuf.nextByte() & 0x80) != 0);
		int length = readLiteralInteger(byteBuf, 1);

		byte[] bytes = new byte[length];

		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = byteBuf.nextByte();
		}

		if (huffmanFlag) {
			bytes = Huffman.decode(bytes);
		}

		return new String(bytes);
	}

	/**
	 * HPACK 값 표현 규격 중 Integer에 해당하는 값을 쓰기 위한 메서드이다.
	 * 
	 * @param outputStream 쓰여질 Stream 인자이다.
	 * @param base         값을 쓰기 위한 배경이되는 Byte에 대한 인자이다.
	 * @param value        실제로 쓰여질 Value에 대한 인자이다.
	 * @param prefix       앞에 건너뛸 바이트를 표현하는 인자이다.
	 * 
	 * @throws IOException 값 작성중 발생하는 오류에 대한 Exception이다.
	 */
	public static void writeLiteralInteger(OutputStream outputStream, byte base, int value, int prefix) throws IOException {
		int max_number = (int) ((int) (Math.pow(2, 8 - prefix)) - 1);

		if (value < max_number) {
			outputStream.write(base | (byte) value);
		} else {
			outputStream.write(base | (byte) max_number);
			value = value - max_number;

			while (value >= 128) {
				outputStream.write((value % 128) + 128);
				value = value % 128;
			}
			outputStream.write((byte) value);
		}
	}

	/**
	 * HPACK 값 표현 규격 중 Integer에 해당하는 값을 읽기 위한 메서드이다.
	 * 
	 * @param byteBuf 읽을 대상이 되는 ByteBuffer 인자이다.
	 * @param prefix  읽을 대상이 되는 Byte의 건너뛸 바이트를 표현하는 인자이다.
	 * 
	 * @return 읽어낸 Integer을 반환한다.
	 * 
	 * @throws IOException 값을 읽는 도중 발생하는 오류에 대한 Exception이다.
	 */
	public static int readLiteralInteger(ByteBuf byteBuf, int prefix) throws IOException {
		int prefixMask = 0xFF >> prefix;

		int maskedValue = (byteBuf.getByte() & prefixMask) & 0xff;
		if (prefixMask == maskedValue) {
			int index = 0;

			while (true) {
				index += 1;
				int value = byteBuf.nextByte() & 0xff;

				if (value > 127) {
					maskedValue += (value - 128) * ((int) Math.pow(128, index - 1));
				} else {
					maskedValue += value * ((int) Math.pow(128, index - 1));
					break;
				}
			}
		}

		return maskedValue;
	}
}
