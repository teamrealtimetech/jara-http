package com.realtimetech.jara.http.header;

import java.util.HashMap;

import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * Header의 Type이 식별 가능할경우를 위한 Header 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public abstract class TypeHeader extends Header {
	/**
	 * Header의 값을 생성하는 클래스이다.
	 * 
	 * @author ParkJeongHwan
	 */
	public static class HeaderValueGenerator {
		private StringBuilder stringBuilder;

		public HeaderValueGenerator() {
			this.stringBuilder = new StringBuilder();
		}

		public void addOptionIfNotNull(String name, String value) {
			if (value != null) {
				if (value.contains(";") || value.contains("=")) {
					addOptionWithDoubleQuote(name, value);
				} else {
					addOptionWithoutDoubleQuote(name, value);
				}
			}
		}

		public void addOptionWithoutDoubleQuote(String name, String value) {
			this.stringBuilder.append(String.format(";%s=%s", name, value));
		}

		public void addOptionWithDoubleQuote(String name, String value) {
			this.stringBuilder.append(String.format(";%s=\"%s\"", name, value));
		}

		public void setValue(String value) {
			stringBuilder.append(value);
		}

		public String getValue() {
			return stringBuilder.toString();
		}
	}

	/**
	 * 인덱싱 여부를 뜻하는 필드이다.
	 */
	private boolean indexing;

	/**
	 * Huffman Encoding 여부를 뜻하는 필드이다.
	 */
	private boolean huffman;

	public TypeHeader(boolean huffman) {
		this(huffman, true);
	}

	public TypeHeader(boolean huffman, boolean indexing) {
		this.huffman = huffman;
		this.indexing = indexing;
	}

	@Override
	public String getName() {
		return getType().getHeaderKey();
	}

	@Override
	public boolean isIndexing() {
		return indexing;
	}

	@Override
	public boolean isHuffman() {
		return huffman;
	}

	/**
	 * 인자로 받은 Value를 Parse하여 새롭게 값을 셋팅하는 메서드이다.
	 * 
	 * @param value 새롭게 변경할 Value 인자이다.
	 */
	public void setValue(String value) {
		tryParse(value);
	}

	@Override
	public String getValue() {
		HeaderValueGenerator headerValueGenerator = new HeaderValueGenerator();

		buildValue(headerValueGenerator);

		return headerValueGenerator.getValue();
	}

	/**
	 * Header Value를 파싱하여 TypeHeader의 실제 값을 구축하는 메서드이다.
	 * 
	 * @param value Header Value 인자이다.
	 * 
	 * @return this를 반환한다.
	 */
	public TypeHeader tryParse(String value) {
		HashMap<String, String> options = new HashMap<String, String>();

		String[] headerDatas = value.split(";");
		value = headerDatas[0];

		if (headerDatas.length >= 2) {
			int index = 0;
			for (String headerOptionData : headerDatas) {
				if (index != 0) {
					String optionName = null;
					String optionValue = null;
					String[] optionSplit = headerOptionData.split("=");

					if (optionSplit.length >= 2) {
						optionName = optionSplit[0].trim();
						optionValue = optionSplit[1].trim();
					} else {
						optionValue = optionSplit[0].trim();
					}

					if (optionName != null) {
						if (optionName.endsWith("\"") && optionName.startsWith("\"")) {
							optionName = optionName.substring(1, optionName.length() - 1);
						}
					}

					if (optionValue != null) {
						if (optionValue.endsWith("\"") && optionValue.startsWith("\"")) {
							optionValue = optionValue.substring(1, optionValue.length() - 1);
						}
					}

					options.put(optionName, optionValue);
				}

				index++;
			}
		}

		parse(value, options);

		return this;
	}

	/**
	 * Header의 실제 Type을 반환하는 메서드이다.
	 * 
	 * @return Header의 Type을 반환한다.
	 */
	public abstract HeaderType getType();

	/**
	 * tryParse 메서드를 통해 만들어진 값을 기반으로 실제 값을 Build하는 메서드이다.
	 * 
	 * @param value 주 Value값에 대한 인자이다.
	 * @param options 부가 옵션 Data에 대한 인자이다.
	 */
	public abstract void parse(String value, HashMap<String, String> options);

	/**
	 * 다시 역으로 Header Value를 Build하는 메서드이다.
	 * 
	 * @param headerValueGenerator 이 인자를 통해서 값을 생성할수 있는 인자이다.
	 */
	public abstract void buildValue(HeaderValueGenerator headerValueGenerator);
}
