package com.realtimetech.jara.http.header.type;

/**
 * HTTP2 Header중 DispositionType에 해당하는 Enum이다.
 * 
 * @author ParkJeongHwan
 */
public enum DispositionType {
	FORMDATA("form-data");

	private String type;

	private DispositionType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	/**
	 * 값을 통해서 특정한 Enum Type을 Parse하는 메서드이다.
	 * 
	 * @param type Parse할 Value 인자이다.
	 * 
	 * @return 해당하는 Enum Type을 반환하거나 찾지 못했을 시 null을 리턴한다.
	 */
	public static DispositionType tryParse(String type) {
		for (DispositionType dispositionType : DispositionType.values()) {
			if (dispositionType.getType().equalsIgnoreCase(type)) {
				return dispositionType;
			}
		}

		return null;
	}
}