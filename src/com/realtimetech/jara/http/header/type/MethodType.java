package com.realtimetech.jara.http.header.type;

/**
 * HTTP2 Header중 MethodType에 해당하는 Enum이다.
 * 
 * @author ParkJeongHwan
 */
public enum MethodType {
	GET("GET"),
	POST("POST"),
	OPTION("OPTION"),
	DELETE("DELETE"),
	UPDATE("UPDATE"),
	PUT("PUT"),

	ALL("?");

	private String value;

	private MethodType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	/**
	 * 값을 통해서 특정한 Enum Type을 Parse하는 메서드이다.
	 * 
	 * @param value Parse할 Value 인자이다.
	 * 
	 * @return 해당하는 Enum Type을 반환하거나 찾지 못했을 시 null을 리턴한다.
	 */
	public static MethodType tryParse(String value) {
		for (MethodType methodType : MethodType.values()) {
			if (methodType.getValue().equalsIgnoreCase(value)) {
				return methodType;
			}
		}

		return null;
	}
}
