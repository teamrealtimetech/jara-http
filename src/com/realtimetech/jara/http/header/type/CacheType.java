package com.realtimetech.jara.http.header.type;

/**
 * HTTP2 Header중 CacheType에 해당하는 Enum이다.
 * 
 * @author ParkJeongHwan
 */
public enum CacheType {
	PUBLIC("public"),
	PRIVATE("private"),
	NO_CACHE("no-cache");

	private String value;

	private CacheType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	/**
	 * 값을 통해서 특정한 Enum Type을 Parse하는 메서드이다.
	 * 
	 * @param value Parse할 Value 인자이다.
	 * 
	 * @return 해당하는 Enum Type을 반환하거나 찾지 못했을 시 null을 리턴한다.
	 */
	public static CacheType tryParse(String value) {
		for (CacheType cacheType : CacheType.values()) {
			if (cacheType.getValue().equalsIgnoreCase(value)) {
				return cacheType;
			}
		}

		return CacheType.PUBLIC;
	}
}