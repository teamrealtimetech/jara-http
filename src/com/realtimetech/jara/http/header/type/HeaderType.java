package com.realtimetech.jara.http.header.type;

import com.realtimetech.jara.http.header.*;
import com.realtimetech.jara.http.reflection.UnsafeAllocator;

/**
 * HTTP2 HeaderType에 해당하는 Enum이다.
 * 
 * @author ParkJeongHwan
 */
public enum HeaderType {
	AUTHORITY(":authority"),
	METHOD(":method", MethodHeader.class),
	PATH(":path"),
	SCHEME(":scheme"),
	STATUS(":status"),
	ACCEPT_CHARSET("accept-charset"),
	ACCEPT_ENCODING("accept-encoding"),
	ACCEPT_LANGUAGE("accept-language"),
	ACCEPT_RANGES("accept-ranges"),
	ACCEPT("accept"),
	ACCESS_CONTROL_ALLOW_ORIGIN("access-control-allow-origin"),
	AGE("age"),
	ALLOW("allow"),
	AUTHORIZATION("authorization"),
	CACHE_CONTROL("cache-control", CacheControlHeader.class),
	CONTENT_DISPOSITION("content-disposition", ContentDispositionHeader.class),
	CONTENT_ENCODING("content-encoding"),
	CONTENT_LANGUAGE("content-language"),
	CONTENT_LENGTH("content-length"),
	CONTENT_LOCATION("content-location"),
	CONTENT_RANGE("content-range"),
	CONTENT_TYPE("content-type", ContentTypeHeader.class),
	COOKIE("cookie"),
	DATE("date"),
	ETAG("etag"),
	EXPECT("expect"),
	EXPIRES("expires"),
	FROM("from"),
	HOST("host"),
	IF_MATCH("if-match"),
	IF_MODIFIED_SINCE("if-modified-since"),
	IF_NONE_MATCH("if-none-match"),
	IF_RANGE("if-range"),
	IF_UNMODIFIED_SINCE("if-unmodified-since"),
	LAST_MODIFIED("last-modified"),
	LINK("link"),
	LOCATION("location"),
	MAX_FORWARDS("max-forwards"),
	PROXY_AUTHENTICATE("proxy-authenticate"),
	PROXY_AUTHORIIZATION("proxy-authorization"),
	RANGE("range"),
	REFERER("referer"),
	REFRESH("refresh"),
	RETRY_AFTER("retry-after"),
	SERVER("server"),
	SET_COOKIE("set-cookie"),
	STRICT_TRANSPORT_SECURITY("strcit-transport-security"),
	TRANSFER_ENCODING("transfer-encoding"),
	USER_AGENT("user-agent"),
	VARY("vary"),
	VIA("via"),
	WWW_AUTHENTICATE("www-authenticate");

	/**
	 * Build에 사용되는 Allocator 필드이다.
	 */
	private static UnsafeAllocator unsafeAllocator = UnsafeAllocator.create();

	private String headerKey;
	private Class<? extends TypeHeader> headerClass;

	private HeaderType(String headerKey) {
		this.headerKey = headerKey;
		this.headerClass = StaticHeader.class;
	}

	private HeaderType(String headerKey, Class<? extends TypeHeader> headerClass) {
		this.headerKey = headerKey;
		this.headerClass = headerClass;
	}

	public String getHeaderKey() {
		return headerKey;
	}

	public TypeHeader buildHeader(String value) {
		if (headerClass != StaticHeader.class) {
			try {
				TypeHeader typeHeader = unsafeAllocator.newInstance(headerClass).tryParse(value);

				return typeHeader;
			} catch (Exception e) {
			}
		}

		return new StaticHeader(this, value);
	}

	public static Header tryBuild(String headerKey, String value) {
		for (HeaderType httpHeaderType : HeaderType.values()) {
			if (httpHeaderType.getHeaderKey().equalsIgnoreCase(headerKey)) {
				return httpHeaderType.buildHeader(value);
			}
		}

		return new CustomHeader(headerKey, value);
	}

	public static HeaderType tryParse(String headerKey) {
		for (HeaderType httpHeaderType : HeaderType.values()) {
			if (httpHeaderType.getHeaderKey().equalsIgnoreCase(headerKey)) {
				return httpHeaderType;
			}
		}

		return null;
	}
}