package com.realtimetech.jara.http.header.type;

import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.network.message.*;
import com.realtimetech.jara.http.reflection.UnsafeAllocator;

/**
 * HTTP2 Header중 MimeType에 해당하는 Enum이다.
 * 
 * @author ParkJeongHwan
 */
public enum MimeType {
	AUDIO_MPEG("audio/mpeg", "mp3"),

	VIDEO_MP4("video/mp4", "mp4"),

	IMAGE_PNG("image/png", "png"),
	IMAGE_JPEG("image/jpeg", "jpe", "jpeg", "jpg"),
	IMAGE_BMP("image/bmp", "bmp"),

	APPLICATION_JAVASCRIPT("application/x-javascript", "js"),
	APPLICATION_JSON("application/json", "json"),
	APPLICATION_OCTECT_STREAM("application/octet-stream", "*"),
	APPLICATION_X_WWW_FORM_URLENCODED(FormContent.class, "application/x-www-form-urlencoded", "form"),

	TEXT_XML("text/xml", "xml"),
	TEXT_HTML("text/html", "html", "htm"),
	TEXT_CSS("text/css", "css"),

	MULTIPART_FORM_DATA(MultipartContent.class, "multipart/form-data", ""),

	TEXT_PLAIN("text/plain", "txt"),

	ALL("", "");

	/**
	 * Build에 사용되는 Allocator 필드이다.
	 */
	private static UnsafeAllocator unsafeAllocator = UnsafeAllocator.create();

	private String[] extensions;
	private String mime;
	private Class<? extends Content> dataClass;

	private MimeType(String mime, String... extensions) {
		this(ByteContent.class, mime, extensions);
	}

	private MimeType(Class<? extends Content> dataClass, String mime, String... extensions) {
		this.mime = mime;
		this.extensions = extensions;
		this.dataClass = dataClass;
	}

	public String getMime() {
		return mime;
	}

	public String[] getExtensions() {
		return extensions;
	}

	public Class<? extends Content> getDataClass() {
		return dataClass;
	}

	/**
	 * 타입에 해당하는 Data Class를 Build하여 Content를 만드는 메서드이다.
	 * 
	 * @param headerSet Build시 요구되는 HeaderSet에 대한 인자이다.
	 * @param bytes     Build할 Byte Array에 대한 인자이다.
	 * 
	 * @return 빌드된 Content를 반환한다.
	 */
	public Content buildContent(HeaderSet headerSet, byte[] bytes) {
		try {
			Content newInstance = unsafeAllocator.newInstance(this.getDataClass());
			newInstance.clear();

			newInstance.read(headerSet, bytes);

			return newInstance;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 값을 통해서 특정한 Enum Type을 Parse하는 메서드이다.
	 * 
	 * @param mime Parse할 Value 인자이다.
	 * 
	 * @return 해당하는 Enum Type을 반환하거나 찾지 못했을 시 null을 리턴한다.
	 */
	public static MimeType tryParse(String mime) {
		for (MimeType mimeType : MimeType.values()) {
			if (mimeType.getMime().equalsIgnoreCase(mime)) {
				return mimeType;
			}
		}

		return null;
	}

	/**
	 * 확장자를 통해서 특정한 Enum Type을 Parse하는 메서드이다.
	 * 
	 * @param extension 검사할 확장자에 대한 인자이다.
	 * 
	 * @return 해당하는 Enum Type을 반환하거나 찾지 못했을 시 null을 리턴한다.
	 */
	public static MimeType tryParseFileExtension(String extension) {
		if (extension.startsWith(".")) {
			extension = extension.substring(1);
		}

		for (MimeType mimeType : MimeType.values()) {
			for (String mimeExtension : mimeType.getExtensions()) {
				if (mimeExtension.equalsIgnoreCase(extension)) {
					return mimeType;
				}
			}
		}

		return MimeType.APPLICATION_OCTECT_STREAM;
	}
}