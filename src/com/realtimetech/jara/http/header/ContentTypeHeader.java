package com.realtimetech.jara.http.header;

import java.nio.charset.Charset;
import java.util.HashMap;

import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.header.type.MimeType;

/**
 * ContentType에 대한 Header의 구현체이다.
 * 
 * @author ParkJeongHwan
 */
public class ContentTypeHeader extends TypeHeader {
	/**
	 * 컨텐츠 Type에 해당하는 필드이다.
	 */
	private MimeType mimeType;

	/**
	 * 컨텐츠의 문자열 인코딩 방법에 해당하는 필드이다.
	 */
	private Charset charset;

	/**
	 * Multipart 컨텐츠일경우 컨텐츠를 구분하는 바운더리 키에 해당하는 필드이다.
	 */
	private String boundaryKey;

	public ContentTypeHeader(MimeType mimeType) {
		this(mimeType, true, true);
	}

	public ContentTypeHeader(MimeType mimeType, boolean huffman) {
		this(mimeType, huffman, true);
	}

	public ContentTypeHeader(MimeType mimeType, boolean huffman, boolean indexing) {
		super(huffman, indexing);

		this.mimeType = mimeType;
	}

	@Override
	public HeaderType getType() {
		return HeaderType.CONTENT_TYPE;
	}

	@Override
	public void parse(String value, HashMap<String, String> options) {
		this.mimeType = MimeType.tryParse(value);

		if (options.containsKey("charset")) {
			this.charset = Charset.forName(options.get("charset"));
		}
		if (options.containsKey("boundary")) {
			this.boundaryKey = options.get("boundary");
		}
	}

	@Override
	public void buildValue(HeaderValueGenerator headerValueGenerator) {
		headerValueGenerator.setValue(mimeType.getMime());
		headerValueGenerator.addOptionIfNotNull("charset", charset != null ? charset.displayName().toLowerCase() : null);
		headerValueGenerator.addOptionIfNotNull("boundary", boundaryKey);
	}

	/**
	 * 컨텐츠 Type을 반환하는 메서드이다.
	 * 
	 * @return 컨텐츠 Type을 반환한다.
	 */
	public MimeType getMimeType() {
		return mimeType;
	}

	/**
	 * 컨텐츠 Type을 설정하는 메서드이다.
	 * 
	 * @param mimeType 설정할 컨텐츠 Type의 인자이다.
	 */
	public void setMimeType(MimeType mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * 컨텐츠의 문자열 인코딩을 반환하는 메서드이다.
	 * 
	 * @return 컨텐츠의 문자열 인코딩을 반환한다.
	 */
	public Charset getCharset() {
		if(charset == null) return HttpServer.DEFAULT_CHARSET;
		
		return charset;
	}

	/**
	 * 컨텐츠의 문자열을 설정하는 메서드이다.
	 * 
	 * @param charset 설정할 컨텐츠 문자열 인코딩의 인자이다.
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	/**
	 * 바운더리 키를 반환하는 메서드이다.
	 * 
	 * @return 바운더리 키를 반환한다.
	 */
	public String getBoundaryKey() {
		return boundaryKey;
	}

	/**
	 * 바운더리 키를 설정하는 메서드이다.
	 * 
	 * @param boundaryKey 설정할 바운더리 키의 인자이다.
	 */
	public void setBoundaryKey(String boundaryKey) {
		this.boundaryKey = boundaryKey;
	}
}
