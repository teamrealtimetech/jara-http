package com.realtimetech.jara.http.header;

import java.util.HashMap;

import com.realtimetech.jara.http.header.type.DispositionType;
import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * ContentDispostion에 대한 Header의 구현체이다.
 * 
 * @author ParkJeongHwan
 */
public class ContentDispositionHeader extends TypeHeader {
	/**
	 * 컨텐츠 처리 Type에 대한 필드이다.
	 */
	private DispositionType dispositionType;

	/**
	 * 컨텐츠의 이름에 대한 필드이다.
	 */
	private String contentName;
	/**
	 * 컨텐츠가 파일인경우 파일의 이름에 대한 필드이다.
	 */
	private String fileName;

	public ContentDispositionHeader(DispositionType dispositionType, String name, String fileName) {
		this(dispositionType, name, fileName, true, true);
	}

	public ContentDispositionHeader(DispositionType dispositionType, String name, String fileName, boolean huffman) {
		this(dispositionType, name, fileName, huffman, true);
	}

	public ContentDispositionHeader(DispositionType dispositionType, String name, String fileName, boolean huffman, boolean indexing) {
		super(huffman, indexing);

		this.dispositionType = dispositionType;
		this.contentName = name;
		this.fileName = fileName;
	}

	/**
	 * 컨텐츠 처리 Type를 반환하는 메서드이다.
	 * 
	 * @return 컨텐츠 처리 Type를 반환한다.
	 */
	public DispositionType getDispositionType() {
		return dispositionType;
	}

	/**
	 * 컨텐츠의 이름을 반환하는 메서드이다.
	 * 
	 * @return 컨텐츠의 이름을 반환한다.
	 */
	public String getContentName() {
		return contentName;
	}

	/**
	 * 컨텐츠의 파일 이름을 반환하는 메서드이다.
	 * 
	 * @return 컨텐츠의 파일 이름을 반환한다.
	 */
	public String getFileName() {
		return fileName;
	}

	@Override
	public HeaderType getType() {
		return HeaderType.CONTENT_DISPOSITION;
	}

	@Override
	public void parse(String value, HashMap<String, String> options) {
		this.dispositionType = DispositionType.tryParse(value);

		if (options.containsKey("name")) {
			this.contentName = options.get("name");
		}

		if (options.containsKey("fileName")) {
			this.fileName = options.get("fileName");
		}
	}

	@Override
	public void buildValue(HeaderValueGenerator headerValueGenerator) {
		headerValueGenerator.setValue(dispositionType.getType());

		headerValueGenerator.addOptionIfNotNull("name", contentName);
		headerValueGenerator.addOptionIfNotNull("fileName", fileName);
	}
}
