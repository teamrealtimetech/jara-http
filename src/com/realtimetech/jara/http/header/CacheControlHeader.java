package com.realtimetech.jara.http.header;

import java.util.HashMap;

import com.realtimetech.jara.http.header.type.CacheType;
import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * CacheControl에 대한 Header의 구현체이다.
 * 
 * @author ParkJeongHwan
 */
public class CacheControlHeader extends TypeHeader {
	/**
	 * Cache Type에 대한 필드입니다.
	 */
	private CacheType cacheType;

	public CacheControlHeader(CacheType cacheType, boolean huffman, boolean indexing) {
		super(huffman, indexing);

		this.cacheType = cacheType;
	}

	/**
	 * Cache Type을 반하는 메서드이다.
	 * 
	 * @return Cache Type을 반환한다.
	 */
	public CacheType getCacheType() {
		return cacheType;
	}
	
	/**
	 * Cache 허용 여부를 반환하는 메서드이다.
	 * 
	 * @return Cache 허용 여부를 반환한다.
	 */
	public boolean isAllowCache() {
		return cacheType != CacheType.NO_CACHE;
	}

	@Override
	public HeaderType getType() {
		return HeaderType.CACHE_CONTROL;
	}

	@Override
	public void parse(String value, HashMap<String, String> options) {
		this.cacheType = CacheType.tryParse(value);
	}

	@Override
	public void buildValue(HeaderValueGenerator headerValueGenerator) {
		headerValueGenerator.setValue(cacheType.getValue());
	}
}
