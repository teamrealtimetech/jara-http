package com.realtimetech.jara.http.header.collection;

import java.util.ArrayList;
import com.realtimetech.jara.http.header.Header;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * Header에 대한 Collection인 Table 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class DynamicHeaderTable implements HeaderTable {
	/**
	 * Header 리스트 필드이다.
	 */
	private ArrayList<Header> headers;

	public DynamicHeaderTable() {
		this(new ArrayList<Header>());
	}

	public DynamicHeaderTable(ArrayList<Header> headers) {
		this.headers = headers;
	}

	/**
	 * Table에 HeaderType과 Value를 Build하여 만든 Header를 Table에 적재하는 메서드이다.
	 * 
	 * @param type  Build할 HeaderType에 대한 인자이다.
	 * @param value Build할 Value에 대한 인자이다.
	 * 
	 * @return Build된 Header를 반환한다.
	 */
	public Header add(HeaderType type, String value) {
		TypeHeader header = type.buildHeader(value);
		headers.add(header);
		return header;
	}

	/**
	 * Header 객체를 Table에 추가하는 메서드이다.
	 * 
	 * @param header 추가할 Header 인자이다.
	 * 
	 * @return Header를 반환한다.
	 */
	public Header add(Header header) {
		headers.add(header);
		return header;
	}

	/**
	 * Header의 Name과 Value를 받아서 CustomHeader를 만든뒤 추가하는 메서드이다.
	 * 
	 * @param name  Header의 Name에 대한 인자이다.
	 * @param value Header의 Value에 대한 인자이다.
	 * 
	 * @return 만들어진 CustomHeader를 반환한다.
	 */
	public Header add(String name, String value) {
		Header header = HeaderType.tryBuild(name, value);
		headers.add(header);
		return header;
	}

	/**
	 * Index를 통해서 Header를 반환해주는 메서드이다.
	 * 
	 * @param index Header를 가져올 Index 인자이다.
	 * 
	 * @return Header를 반환한다.
	 */
	public Header get(int index) {
		final int staticTableSize = StaticHeaderTable.STATIC_TABLE.size();
		
		if (staticTableSize >= index) {
			return StaticHeaderTable.STATIC_TABLE.get(index);
		} else {
			return headers.get((headers.size() - (index - staticTableSize)));
		}
	}

	/**
	 * Header를 통해서 Index를 반환해주는 메서드이다.
	 * 
	 * @param header Index를 가져올 Header 인자이다.
	 * 
	 * @return Index를 반환한다.
	 */
	public int get(Header header) {
		final int staticTableSize = StaticHeaderTable.STATIC_TABLE.size();
		int staticIndexOf = StaticHeaderTable.STATIC_TABLE.get(header);
		
		if(staticIndexOf != -1) {
			return staticIndexOf;
		}
		
		int dynamicIndexOf = headers.indexOf(header);
		
		if (dynamicIndexOf != -1) {
			return staticTableSize + (headers.size() - dynamicIndexOf);
		}

		return dynamicIndexOf;
	}

	/**
	 * Table을 복사한다.
	 */
	public DynamicHeaderTable clone() {
		return new DynamicHeaderTable(this.headers);
	}

	@Override
	public int size() {
		return StaticHeaderTable.STATIC_TABLE.size() + this.headers.size();
	}
}
