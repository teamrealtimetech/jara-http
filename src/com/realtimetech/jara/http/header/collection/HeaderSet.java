package com.realtimetech.jara.http.header.collection;

import java.util.LinkedList;
import java.util.List;

import com.realtimetech.jara.http.header.CustomHeader;
import com.realtimetech.jara.http.header.Header;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * Header에 대한 Collection인 Set 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class HeaderSet {
	/**
	 * Header 리스트 필드이다.
	 */
	private List<Header> headers;

	public HeaderSet() {
		this.headers = new LinkedList<Header>();
	}

	/**
	 * Header 리스트를 반환하는 메서드이다.
	 * 
	 * @return Header 리스트를 반환한다.
	 */
	public List<Header> getHeaders() {
		return headers;
	}

	/**
	 * Header 객체를 Set에 추가하는 메서드이다.
	 * 
	 * @param header 추가할 Header 인자이다.
	 * 
	 * @return this를 반환한다.
	 */
	public HeaderSet add(Header header) {
		headers.add(header);
		return this;
	}

	/**
	 * HeaderType과 Value를 받아서 TypeHeader를 Build한 뒤 추가하는 메서드이다.
	 * 
	 * @param type  Build할 HeaderType에 대한 인자이다.
	 * @param value Build할 Value에 대한 인자이다.
	 * 
	 * @return Build된 Header를 반환한다.
	 */
	public TypeHeader add(HeaderType type, String value) {
		TypeHeader buildHeader = type.buildHeader(value);
		headers.add(buildHeader);
		return buildHeader;
	}

	/**
	 * Header의 Name과 Value를 받아서 CustomHeader를 만든뒤 추가하는 메서드이다.
	 * 
	 * @param name  Header의 Name에 대한 인자이다.
	 * @param value Header의 Value에 대한 인자이다.
	 * 
	 * @return 만들어진 CustomHeader를 반환한다.
	 */
	public Header add(String name, String value) {
		CustomHeader header = new CustomHeader(name, value);
		headers.add(header);
		return header;
	}

	/**
	 * HeaderType를 교체하는 메서드이다.
	 * 
	 * @param header 교체할 Header에 대한 인자이다.
	 * 
	 * @return this를 반환한다.
	 */
	public HeaderSet set(TypeHeader header) {
		TypeHeader typeHeader = get(header.getType());

		if (typeHeader == null) {
			add(header);
		} else {
			headers.remove(typeHeader);
			headers.add(header);
		}

		return this;
	}

	/**
	 * HeaderType에 해당하는 Header를 가져오는 메서드이다.
	 * 
	 * @param headerType 찾을 HeaderType 인자이다.
	 * 
	 * @return 찾은 TypeHeader를 반환한다.
	 */
	public TypeHeader get(HeaderType headerType) {
		for (Header header : headers) {
			if (header instanceof TypeHeader) {
				TypeHeader typeHeader = (TypeHeader) header;

				if (typeHeader.getType() == headerType) {
					return typeHeader;
				}
			}
		}
		return null;
	}

	/**
	 * HeaderType에 해당하는 Header를 찾은 뒤 Value를 새로 Build하는 메서드이다.
	 * 
	 * @param type  Build할 HeaderType에 대한 인자이다.
	 * @param value Build할 Value에 대한 인자이다.
	 * 
	 * @return Build된 TypeHeader를 반환한다.
	 */
	public TypeHeader set(HeaderType type, String value) {
		TypeHeader typeHeader = get(type);

		if (typeHeader == null) {
			typeHeader = add(type, value);
		} else {
			typeHeader.setValue(value);
		}

		return typeHeader;
	}

	/**
	 * Header List의 모든 Header의 String을 List에 넣어서 반환하는 메서드이다.
	 * 
	 * @return List를 반환한다.
	 */
	public List<String> toList() {
		List<String> headers = new LinkedList<String>();
		for (Header header : this.headers) {
			headers.add(header.toString());
		}
		return headers;
	}
}
