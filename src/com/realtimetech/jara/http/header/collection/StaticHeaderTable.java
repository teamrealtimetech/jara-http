package com.realtimetech.jara.http.header.collection;

import java.util.HashMap;

import com.realtimetech.jara.http.header.Header;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * Header에 대한 Collection인 Table 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class StaticHeaderTable implements HeaderTable {
	/**
	 * HTTP2 Protocol에 규약된 고정된 Table Static 필드이다.
	 */
	public static final StaticHeaderTable STATIC_TABLE;
	static {
		STATIC_TABLE = new StaticHeaderTable();

		STATIC_TABLE.add(HeaderType.AUTHORITY, "");
		STATIC_TABLE.add(HeaderType.METHOD, "GET");
		STATIC_TABLE.add(HeaderType.METHOD, "POST");
		STATIC_TABLE.add(HeaderType.PATH, "/");
		STATIC_TABLE.add(HeaderType.PATH, "/index.html");
		STATIC_TABLE.add(HeaderType.SCHEME, "http");
		STATIC_TABLE.add(HeaderType.SCHEME, "https");
		STATIC_TABLE.add(HeaderType.STATUS, "200");
		STATIC_TABLE.add(HeaderType.STATUS, "204");
		STATIC_TABLE.add(HeaderType.STATUS, "206");
		STATIC_TABLE.add(HeaderType.STATUS, "304");
		STATIC_TABLE.add(HeaderType.STATUS, "400");
		STATIC_TABLE.add(HeaderType.STATUS, "404");
		STATIC_TABLE.add(HeaderType.STATUS, "500");
		STATIC_TABLE.add(HeaderType.ACCEPT_CHARSET, "");
		STATIC_TABLE.add(HeaderType.ACCEPT_ENCODING, "gzip, deflate");
		STATIC_TABLE.add(HeaderType.ACCEPT_LANGUAGE, "");
		STATIC_TABLE.add(HeaderType.ACCEPT_RANGES, "");
		STATIC_TABLE.add(HeaderType.ACCEPT, "");
		STATIC_TABLE.add(HeaderType.ACCESS_CONTROL_ALLOW_ORIGIN, "");
		STATIC_TABLE.add(HeaderType.AGE, "");
		STATIC_TABLE.add(HeaderType.ALLOW, "");
		STATIC_TABLE.add(HeaderType.AUTHORIZATION, "");
		STATIC_TABLE.add(HeaderType.CACHE_CONTROL, "");
		STATIC_TABLE.add(HeaderType.CONTENT_DISPOSITION, "");
		STATIC_TABLE.add(HeaderType.CONTENT_ENCODING, "");
		STATIC_TABLE.add(HeaderType.CONTENT_LANGUAGE, "");
		STATIC_TABLE.add(HeaderType.CONTENT_LENGTH, "");
		STATIC_TABLE.add(HeaderType.CONTENT_LOCATION, "");
		STATIC_TABLE.add(HeaderType.CONTENT_RANGE, "");
		STATIC_TABLE.add(HeaderType.CONTENT_TYPE, "");
		STATIC_TABLE.add(HeaderType.COOKIE, "");
		STATIC_TABLE.add(HeaderType.DATE, "");
		STATIC_TABLE.add(HeaderType.ETAG, "");
		STATIC_TABLE.add(HeaderType.EXPECT, "");
		STATIC_TABLE.add(HeaderType.EXPIRES, "");
		STATIC_TABLE.add(HeaderType.FROM, "");
		STATIC_TABLE.add(HeaderType.HOST, "");
		STATIC_TABLE.add(HeaderType.IF_MATCH, "");
		STATIC_TABLE.add(HeaderType.IF_MODIFIED_SINCE, "");
		STATIC_TABLE.add(HeaderType.IF_NONE_MATCH, "");
		STATIC_TABLE.add(HeaderType.IF_RANGE, "");
		STATIC_TABLE.add(HeaderType.IF_UNMODIFIED_SINCE, "");
		STATIC_TABLE.add(HeaderType.LAST_MODIFIED, "");
		STATIC_TABLE.add(HeaderType.LINK, "");
		STATIC_TABLE.add(HeaderType.LOCATION, "");
		STATIC_TABLE.add(HeaderType.MAX_FORWARDS, "");
		STATIC_TABLE.add(HeaderType.PROXY_AUTHENTICATE, "");
		STATIC_TABLE.add(HeaderType.PROXY_AUTHORIIZATION, "");
		STATIC_TABLE.add(HeaderType.RANGE, "");
		STATIC_TABLE.add(HeaderType.REFERER, "");
		STATIC_TABLE.add(HeaderType.REFRESH, "");
		STATIC_TABLE.add(HeaderType.RETRY_AFTER, "");
		STATIC_TABLE.add(HeaderType.SERVER, "");
		STATIC_TABLE.add(HeaderType.SET_COOKIE, "");
		STATIC_TABLE.add(HeaderType.STRICT_TRANSPORT_SECURITY, "");
		STATIC_TABLE.add(HeaderType.TRANSFER_ENCODING, "");
		STATIC_TABLE.add(HeaderType.USER_AGENT, "");
		STATIC_TABLE.add(HeaderType.VARY, "");
		STATIC_TABLE.add(HeaderType.VIA, "");
		STATIC_TABLE.add(HeaderType.WWW_AUTHENTICATE, "");
	}

	/**
	 * Header 리스트 필드이다.
	 */
	private HashMap<Integer, Header> headers;

	/**
	 * Table에 대한 Global Index 필드이다.
	 */
	private int globalIndex;

	public StaticHeaderTable() {
		this(new HashMap<Integer, Header>(), 1);
	}

	public StaticHeaderTable(HashMap<Integer, Header> headers, int globalIndex) {
		this.headers = headers;
		this.globalIndex = globalIndex;
	}

	/**
	 * Table에 HeaderType과 Value를 Build하여 만든 Header를 Table에 적재하는 메서드이다.
	 * 
	 * @param type  Build할 HeaderType에 대한 인자이다.
	 * @param value Build할 Value에 대한 인자이다.
	 * 
	 * @return Build된 Header를 반환한다.
	 */
	public Header add(HeaderType type, String value) {
		TypeHeader header = type.buildHeader(value);
		headers.put(globalIndex++, header);
		return header;
	}

	/**
	 * Header 객체를 Table에 추가하는 메서드이다.
	 * 
	 * @param header 추가할 Header 인자이다.
	 * 
	 * @return Header를 반환한다.
	 */
	public Header add(Header header) {
		headers.put(globalIndex++, header);
		return header;
	}

	/**
	 * Header의 Name과 Value를 받아서 CustomHeader를 만든뒤 추가하는 메서드이다.
	 * 
	 * @param name  Header의 Name에 대한 인자이다.
	 * @param value Header의 Value에 대한 인자이다.
	 * 
	 * @return 만들어진 CustomHeader를 반환한다.
	 */
	public Header add(String name, String value) {
		Header header = HeaderType.tryBuild(name, value);
		headers.put(globalIndex++, header);
		return header;
	}

	/**
	 * Index를 통해서 Header를 반환해주는 메서드이다.
	 * 
	 * @param index Header를 가져올 Index 인자이다.
	 * 
	 * @return Header를 반환한다.
	 */
	public Header get(int index) {
		return headers.get(index);
	}

	/**
	 * Header를 통해서 Index를 반환해주는 메서드이다.
	 * 
	 * @param header Index를 가져올 Header 인자이다.
	 * 
	 * @return Index를 반환한다.
	 */
	public int get(Header header) {
		for (int key : headers.keySet()) {
			if (headers.get(key) == header) {
				return key;
			}
		}

		return -1;
	}

	/**
	 * Table을 복사한다.
	 */
	public StaticHeaderTable clone() {
		return new StaticHeaderTable(new HashMap<Integer, Header>(this.headers), globalIndex);
	}

	@Override
	public int size() {
		return this.headers.size();
	}
}
