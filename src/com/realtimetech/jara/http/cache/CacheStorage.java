package com.realtimetech.jara.http.cache;

import java.util.HashMap;

import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.header.CacheControlHeader;
import com.realtimetech.jara.http.header.TypeHeader;
import com.realtimetech.jara.http.header.type.HeaderType;
import com.realtimetech.jara.http.network.message.request.HttpRequest;

/**
 * Cache Data를 관리해주는 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class CacheStorage {
	/**
	 * Cache Data를 가져올 수 있도록 하는 추상화 클래스이다.
	 * 
	 * @author ParkJeongHwan
	 *
	 * @param <T> Cache Data의 형태를 지정합니다.
	 */
	public static abstract class CacheValueGetter<T> {
		public abstract Object getValue();

		public abstract boolean isRefresh(T object);
	}

	private HttpServer httpServer;

	/**
	 * Cache Data를 저장하는 Map이다, 이중 맵으로 구성되어 있다.
	 */
	private HashMap<Object, HashMap<String, HashMap<Object, Object>>> cachedData;

	public CacheStorage(HttpServer httpServer) {
		this.httpServer = httpServer;
		this.cachedData = new HashMap<Object, HashMap<String, HashMap<Object, Object>>>();
	}

	/**
	 * 해당하는 HttpServer 객체를 반환하는 메서드이다.
	 * 
	 * @return HttpServer를 반환한다.
	 */
	public HttpServer getHttpServer() {
		return httpServer;
	}

	/**
	 * 캐싱된 테이블을 가져오는 내부 메서드이다.
	 * 
	 * @param object 캐싱에 대한 분류 중 Object 분류에 대한 인자이다.
	 * @param title  캐싱에 대한 분류 중 Title 분류에 대한 인자이다.
	 * 
	 * @return 캐싱 테이블을 반환한다.
	 */
	private synchronized HashMap<Object, Object> getCachedTable(Object object, String title) {
		if (!cachedData.containsKey(object)) {
			cachedData.put(object, new HashMap<String, HashMap<Object, Object>>());
		}

		HashMap<String, HashMap<Object, Object>> hashMap = cachedData.get(object);

		if (!hashMap.containsKey(title)) {
			hashMap.put(title, new HashMap<>());
		}

		return hashMap.get(title);
	}

	/**
	 * 캐싱된 Data가 존재하는지 여부에 대한 메서드이다.
	 * 
	 * @param httpRequest 캐싱여부를 결정하는 Header가 담긴 HttpRequest에 대한 인자이다.
	 * @param object      캐싱에 대한 분류 중 Object 분류에 대한 인자이다.
	 * @param title       캐싱에 대한 분류 중 Title 분류에 대한 인자이다.
	 * @param key         캐싱에 대한 최종 분류인 Key 인자이다.
	 * 
	 * @return 존재 여부를 반환한다.
	 */
	public boolean hasCachedData(HttpRequest httpRequest, Object object, String title, Object key) {
		TypeHeader typeHeader = httpRequest.getHeaderSet().get(HeaderType.CACHE_CONTROL);

		if (typeHeader instanceof CacheControlHeader) {
			CacheControlHeader cacheControlHeader = (CacheControlHeader) typeHeader;

			if (!cacheControlHeader.isAllowCache()) {
				return false;
			}
		}

		return getCachedTable(object, title).containsKey(key);
	}

	/**
	 * 캐싱된 Data를 가져오는 메서드이다.
	 * 
	 * @param httpRequest  캐싱여부를 결정하는 Header가 담긴 HttpRequest에 대한 인자이다.
	 * @param object       캐싱에 대한 분류 중 Object 분류에 대한 인자이다.
	 * @param title        캐싱에 대한 분류 중 Title 분류에 대한 인자이다.
	 * @param key          캐싱에 대한 최종 분류인 Key 인자이다.
	 * @param defaultValue 값이 없을시 사용되는 CacheValueGetter 인자이다.
	 * @param              <T> Cache Data의 형태를 지정합니다.
	 * 
	 * @return 캐싱된 Data를 반환한다.
	 */
	@SuppressWarnings("unchecked")
	public <T> T getCachedData(HttpRequest httpRequest, Object object, String title, Object key, CacheValueGetter<T> defaultValue) {
		TypeHeader typeHeader = httpRequest.getHeaderSet().get(HeaderType.CACHE_CONTROL);

		if (typeHeader instanceof CacheControlHeader) {
			CacheControlHeader cacheControlHeader = (CacheControlHeader) typeHeader;

			if (!cacheControlHeader.isAllowCache()) {
				return (T) defaultValue.getValue();
			}
		}

		HashMap<Object, Object> cachedTable = getCachedTable(object, title);
		Object value = cachedTable.get(key);

		if (value == null || defaultValue.isRefresh((T) value)) {
			cachedTable.put(key, defaultValue.getValue());
		}

		Object resultObject = cachedTable.get(key);
		return resultObject == null ? null : (T) resultObject;
	}

	/**
	 * 특정 캐싱 Data를 제거하는 메서드이다.
	 * 
	 * @param object 캐싱에 대한 분류 중 Object 분류에 대한 인자이다.
	 * @param title  캐싱에 대한 분류 중 Title 분류에 대한 인자이다.
	 * @param key    캐싱에 대한 최종 분류인 Key 인자이다.
	 */
	public void clearCachedData(Object object, String title, Object key) {
		getCachedTable(object, title).remove(key);
	}

	/**
	 * 특정 캐싱 Data를 추가하는 메서드이다.
	 * 
	 * @param object 캐싱에 대한 분류 중 Object 분류에 대한 인자이다.
	 * @param title  캐싱에 대한 분류 중 Title 분류에 대한 인자이다.
	 * @param key    캐싱에 대한 최종 분류인 Key 인자이다.
	 * @param data   캐싱 Data이다.
	 */
	public void addCachedData(Object object, String title, Object key, Object data) {
		getCachedTable(object, title).put(key, data);
	}

	/**
	 * 모든 캐싱 Data를 제거하는 메서드이다.
	 */
	public void clearAllCache() {
		for (Object object : cachedData.keySet()) {
			for (String title : cachedData.get(object).keySet()) {
				cachedData.get(object).get(title).clear();
			}
			cachedData.get(object).clear();
		}
		cachedData.clear();
	}
}
