package com.realtimetech.jara.http.cache;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import com.realtimetech.jara.http.HttpServer;
import com.realtimetech.jara.http.cache.CacheStorage.CacheValueGetter;
import com.realtimetech.jara.http.network.message.request.HttpRequest;

/**
 * 파일의 바이트들을 Cache를 통해서 읽어오고 관리하는 클래스이다.
 * 
 * @author ParkJeongHwan
 */
public class FileStorage {
	/**
	 * 파일에 대한 Cache 컨트롤을 위한 정보가 담긴 클래스이다.
	 * 
	 * @author ParkJeongHwan
	 */
	public static class FileInfo {
		private File file;
		private byte[] bytes;
		private long length;
		private long lastModifiedTime;

		public FileInfo(File file, byte[] bytes) {
			this.file = file;
			this.bytes = bytes;
			this.length = file.length();
			this.lastModifiedTime = file.lastModified();
		}

		public byte[] getBytes() {
			return bytes;
		}

		public File getFile() {
			return file;
		}

		public long getLastModifiedTime() {
			return lastModifiedTime;
		}

		public long getLength() {
			return length;
		}

		public boolean isChanged() {
			if (this.length == file.length() && this.lastModifiedTime == file.lastModified()) {
				return false;
			}

			return true;
		}
	}

	/**
	 * 파일의 Data를 Cache를 통해서 가져오는 CacheValueGetter의 구현체이다.
	 * 
	 * @author ParkJeongHwan
	 */
	private class CacheFileGetter extends CacheValueGetter<FileInfo> {
		private File file;

		public CacheFileGetter(File file) {
			this.file = file;
		}

		@Override
		public FileInfo getValue() {
			try {
				return new FileInfo(file, Files.readAllBytes(file.toPath()));
			} catch (IOException e) {
				return null;
			}
		}

		@Override
		public boolean isRefresh(FileInfo fileInfo) {
			return fileInfo.isChanged();
		}

	}

	private HttpServer httpServer;

	public FileStorage(HttpServer httpServer) {
		this.httpServer = httpServer;
	}

	/**
	 * Cache 컨트롤링을 이용해서 파일의 Data를 반환하는 메서드이다.
	 * 
	 * @param httpRequest 캐싱여부를 결정하는 Header가 담긴 HttpRequest에 대한 인자이다.
	 * @param file        읽을 파일에 대한 인자이다.
	 * @return 파일의 Data를 리턴한다, 만일 파일이 존재하지 않을경우 null을 반환한다.
	 */
	public byte[] getFileBytes(HttpRequest httpRequest, File file) {
		return httpServer.getCacheStorage().<FileInfo>getCachedData(httpRequest, this, "files", file.getPath(), new CacheFileGetter(file)).getBytes();
	}

	/**
	 * 해당하는 HttpServer 객체를 반환하는 메서드이다.
	 * 
	 * @return HttpServer를 반환한다.
	 */
	public HttpServer getHttpServer() {
		return httpServer;
	}
}
