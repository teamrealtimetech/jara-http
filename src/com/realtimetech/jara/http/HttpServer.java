package com.realtimetech.jara.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

import com.realtimetech.carbon.Carbon;
import com.realtimetech.carbon.Version;
import com.realtimetech.carbon.logger.Logger;
import com.realtimetech.carbon.logger.level.Level;
import com.realtimetech.carbon.properties.OptionProperties;
import com.realtimetech.carbon.server.Server;
import com.realtimetech.carbon.service.Service;
import com.realtimetech.jara.http.cache.CacheStorage;
import com.realtimetech.jara.http.cache.CacheStorage.CacheValueGetter;
import com.realtimetech.jara.http.cache.FileStorage;
import com.realtimetech.jara.http.connection.Connection;
import com.realtimetech.jara.http.header.type.MethodType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.response.HttpResponse;
import com.realtimetech.jara.http.route.RequestRouter;
import com.realtimetech.jara.http.util.StreamUtil;

public class HttpServer extends Server {
	private static class CachedRouteResultGetter extends CacheValueGetter<Boolean> {
		private HttpRequest httpRequest;
		private RequestRouter requestRouter;

		public CachedRouteResultGetter(HttpRequest httpRequest, RequestRouter requestRouter) {
			this.httpRequest = httpRequest;
			this.requestRouter = requestRouter;
		}

		@Override
		public Boolean getValue() {
			boolean matchedRequestUrl = true;
			String[] requestUrl = httpRequest.getPath().split("/");
			String[] routeUrl = requestRouter.requestUrl().split("/");

			if (routeUrl.length == 0 && requestUrl.length == 0) {
				matchedRequestUrl = true;
			} else {
				if (routeUrl.length <= requestUrl.length) {
					int routeIndex = 0;

					for (int index = 0; index < requestUrl.length; index++) {
						boolean wildFlag = routeIndex == routeUrl.length - 1 && routeUrl[routeIndex].equals("*");
						boolean normalFlag = routeUrl.length > routeIndex && requestUrl[index].equals(routeUrl[routeIndex]);

						if (wildFlag || normalFlag) {
							if (routeIndex < routeUrl.length - 1) {
								routeIndex++;
							}
						} else {
							matchedRequestUrl = false;
							break;
						}
					}
				}
			}

			return matchedRequestUrl;
		}

		@Override
		public boolean isRefresh(Boolean object) {
			return false;
		}
	}

	public static final String NAME = "JaraServer/0.2";
	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.RFC_1123_DATE_TIME.withZone(ZoneOffset.UTC);

	private Logger logger = Carbon.getInstance().getLogger(HttpServer.class);

	private int maxConnection = 10;
	private int maxStreamThreadPerConnection = 5;

	private CacheStorage cacheStorage;
	private FileStorage fileStorage;

	private ArrayList<RequestRouter> requestRouters;

	private int port;
	private SSLServerSocket serverSocket;

	private String keyStoreFileName;
	private String keyStorePassword;

	public HttpServer(int port, String keyStoreFileName, String keyStorePassword) {
		this.port = port;

		this.keyStoreFileName = keyStoreFileName;
		this.keyStorePassword = keyStorePassword;

		this.cacheStorage = new CacheStorage(this);
		this.fileStorage = new FileStorage(this);
		this.requestRouters = new ArrayList<RequestRouter>();
	}

	public HttpServer registerRouter(RequestRouter requestRouter) {
		this.requestRouters.add(requestRouter);

		return this;
	}

	public CacheStorage getCacheStorage() {
		return cacheStorage;
	}

	public FileStorage getFileStorage() {
		return fileStorage;
	}

	public int getPort() {
		return port;
	}

	public HttpServer setPort(int port) {
		this.port = port;

		return this;
	}

	public int getMaxConnection() {
		return maxConnection;
	}

	public HttpServer setMaxConnection(int maxConnection) {
		this.maxConnection = maxConnection;

		return this;
	}

	public int getMaxStreamThreadPerConnection() {
		return maxStreamThreadPerConnection;
	}

	public HttpServer setMaxStreamThreadPerConnection(int maxStreamThreadPerConnection) {
		this.maxStreamThreadPerConnection = maxStreamThreadPerConnection;

		return this;
	}

	@Override
	public String getName() {
		return "Jara-HTTP";
	}

	public Version createVersion() {
		return new Version(0, 0, 1);
	}

	@Override
	protected String getPropertiesFileName() {
		return "http.properties";
	}

	@Override
	protected boolean onInitialize() {
		System.setProperty("javax.net.ssl.keyStore", keyStoreFileName);
		System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);

		try {
			SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();

			this.serverSocket = (SSLServerSocket) ssf.createServerSocket(port);

			SSLParameters sslp = serverSocket.getSSLParameters();
			String[] suites = sslp.getCipherSuites();
			sslp.setCipherSuites(suites);
			sslp.setUseCipherSuitesOrder(true);
			sslp.setProtocols(new String[] { "TLSv1.2" });
			sslp.setApplicationProtocols(new String[] { "h2" });

			this.serverSocket.setSSLParameters(sslp);

			return true;
		} catch (IOException e) {
			logger.log(Level.ERROR, "Can't open server socket because already port using.");
		}

		return false;
	}

	@Override
	protected boolean onStart() {
		bindService(new Service() {
			final byte[] PREFACE = new byte[] { 0x50, 0x52, 0x49, 0x20, 0x2a, 0x20, 0x48, 0x54, 0x54, 0x50, 0x2f, 0x32, 0x2e, 0x30, 0x0d, 0x0a, 0x0d, 0x0a, 0x53, 0x4d, 0x0d, 0x0a, 0x0d, 0x0a };

			@Override
			public void onBind() {

			}

			@Override
			public void onService() throws InterruptedException {
				try {
					Socket socket = serverSocket.accept();

					InputStream inputStream = socket.getInputStream();

					byte[] readOneTime = new byte[PREFACE.length];

					StreamUtil.readFully(inputStream, readOneTime, 0, PREFACE.length);

					if (StreamUtil.compareArray(readOneTime, PREFACE)) {
						Connection connection = new Connection(HttpServer.this, socket);

						connection.active();
					} else {
						logger.log(Level.ERROR, "A client with unknown Handshake connected.");
					}
				} catch (IOException e) {
					logger.log(Level.ERROR, "An exception occurred while waiting for a connection.");
				}
			}

			@Override
			public void onUnbind() {
				try {
					serverSocket.close();
				} catch (IOException e) {
					logger.log(Level.ERROR, "An exception occurred while shutting down the server.");
				}
			}

			@Override
			public String getProviderName() {
				return "Listen";
			}
		});

		return true;
	}

	@Override
	protected boolean onStop() {
		return true;
	}

	@Override
	protected Server newServerInstance(OptionProperties optionProperties) {
		return new HttpServer(optionProperties.get("port", 443), optionProperties.get("keyStore", "server.key"), optionProperties.get("keyStorePassword", "123456"));
	}

	public void processRequest(HttpRequest httpRequest, HttpResponse httpResponse) {
		if (httpRequest != null) {
			RequestRouter[] routers = null;

			synchronized (requestRouters) {
				routers = requestRouters.toArray(new RequestRouter[requestRouters.size()]);
			}
			for (RequestRouter requestRouter : routers) {
				if (requestRouter.requestMimeType() == MimeType.ALL || httpRequest.getMimeType() == requestRouter.requestMimeType()) {
					if (requestRouter.requestMethod() == MethodType.ALL || httpRequest.getMethodType() == requestRouter.requestMethod()) {
						boolean matchedRequestUrl = cacheStorage.getCachedData(httpRequest, requestRouter, "route_result", httpRequest.getPath(), new CachedRouteResultGetter(httpRequest, requestRouter));

						if (matchedRequestUrl) {
							if (requestRouter.onProcess(httpRequest, httpResponse)) {
								httpResponse.setResponseCode(200);
								return;
							}
						}
					}
				}
			}
		}
	}
}
