package com.realtimetech.jara.http.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * Stream에 대한 다양한 기능이 포함된 Util Class이다.
 * 
 * @author ParkJeongHwam
 */
public class StreamUtil {
	
	/**
	 * Stream에서 Byte를 Read하는 메서드이다.
	 * 
	 * @param inputStream 대상이되는 Stream 인자이다.
	 * @param bytes       결과를 적재할 Byte Array에 대한 인자이다.
	 * @param offset      Array에 대한 Offset 인자이다.
	 * @param size        읽을 크기에 대한 인자이다.
	 * 
	 * @throws IOException Stream에서 발생한 Exception이다.
	 */
	public static void readFully(InputStream inputStream, byte[] bytes, int offset, int size) throws IOException {
		long totalRead = 0;
		do {
			int read = inputStream.read(bytes, (int) totalRead + offset, size - (int) totalRead);
			if (read > 0)
				totalRead += read;
		} while (totalRead < size);
	}

	/**
	 * 두 Array를 비교하는 메서드이다.
	 * 
	 * @param array1 대상이 되는 Array 인자이다.
	 * @param array2 대상이 되는 Array 인지이다.
	 * 
	 * @return 비교 결과를 반환 합니다.
	 */
	public static boolean compareArray(byte[] array1, byte[] array2) {
		if (array1.length == array2.length) {
			for (int i = 0; i < array1.length; i++) {
				if (array1[i] != array2[i]) {
					return false;
				}
			}

			return true;
		}

		return false;
	}
}
