package com.realtimetech.jara.http.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.realtimetech.jara.http.header.CustomHeader;
import com.realtimetech.jara.http.header.collection.HeaderSet;
import com.realtimetech.jara.http.header.type.HeaderType;

/**
 * HTTP 1.0 기반의 StreamUtil Class이다, 이 Class는 개선이 필요하다.
 * 
 * @author ParkJeongHwan
 */
public class HttpStreamUtil {
	public static HashMap<String, String> readParameters(String string, Charset charset) {
		String[] parameterSplit = string.split("&");
		HashMap<String, String> parameters = new HashMap<String, String>();

		for (String parameter : parameterSplit) {
			String[] dataSplit = parameter.split("=");
			if (dataSplit.length == 2) {
				parameters.put(decodeUrl(charset, dataSplit), URLDecoder.decode(dataSplit[1], charset));
			} else {
				parameters.put("", decodeUrl(charset, dataSplit));
			}
		}

		return parameters;
	}

	public static String wrtieParameters(HashMap<String, String> parameters, Charset charset) {
		StringBuilder stringBuilder = new StringBuilder();
		
		int index = 0;
		for (String key : parameters.keySet()) {
			String value = parameters.get(key);
			
			if(index != 0) {
				stringBuilder.append("&");
			}
			
			if(!key.equals("")) {
				stringBuilder.append(encodeUrl(charset, key));
				stringBuilder.append("=");
				stringBuilder.append(encodeUrl(charset, value));
			}else {
				stringBuilder.append(encodeUrl(charset, value));
			}
		}
		return stringBuilder.toString();
	}

	private static String encodeUrl(Charset charset, String key) {
		return URLEncoder.encode(key, charset).replace("+", "%20");
	}

	private static String decodeUrl(Charset charset, String[] dataSplit) {
		return URLDecoder.decode(dataSplit[0], charset).replace("%20", "+");
	}

	public static HeaderSet readHeaders(List<String> request) {
		HeaderSet headerSet = new HeaderSet();

		for (String header : request) {
			String[] headerSplit = header.split(": ");

			String headerName = headerSplit[0];
			String headerValue =  headerSplit[1];

			HeaderType headerType = HeaderType.tryParse(headerName);

			if (headerType != null) {
				headerSet.add(headerType.buildHeader(headerValue));
			} else {
				headerSet.add(new CustomHeader(headerName, headerValue));
			}
		}

		return headerSet;
	}

	public static byte[] readByteAtBytes(InputStream inputStream, byte[] endBytes) throws IOException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ByteArrayOutputStream subOutputStream = new ByteArrayOutputStream();

		try {
			byte[] readByte = new byte[1];
			int endIndex = 0;
			boolean readMore = true;

			while (true) {
				if (readMore) {
					if (inputStream.read(readByte) == -1) {
						break;
					}
				} else {
					readMore = true;
				}

				if (endBytes[endIndex] == readByte[0]) {
					subOutputStream.write(readByte);
					endIndex++;

					if (endBytes.length == endIndex) {
						break;
					}
				} else {
					if (endIndex != 0) {
						readMore = false;

						outputStream.write(subOutputStream.toByteArray());
						endIndex = 0;
						subOutputStream.reset();
					} else {
						outputStream.write(readByte[0]);
					}
				}
			}

			return outputStream.toByteArray();
		} finally {
			outputStream.close();
			subOutputStream.close();
		}
	}

	public static byte[] readByteFully(InputStream inputStream, int size) throws IOException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		int receivedByte = 0;

		byte[] readByte = new byte[128];
		while (receivedByte < size) {
			int count = inputStream.read(readByte);

			outputStream.write(readByte, 0, count);
			receivedByte += count;
		}

		return outputStream.toByteArray();
	}

	public static List<String> readHeaderStringList(InputStream inputStream, Charset charset) throws IOException {
		List<String> request = new ArrayList<String>();

		byte[] endByte = "\r\n\r\n".getBytes();

		String string = new String(readByteAtBytes(inputStream, endByte));
		String[] lines = string.split("\r\n");

		for (String line : lines) {
			if (!line.equals("")) {
				request.add(line);
			}
		}

		return request;
	}

	public static void writeHeaderStringList(OutputStream outputStream, List<String> headers, Charset charset) throws IOException {
		byte[] endByte = "\r\n".getBytes();

		for (String line : headers) {
			outputStream.write(line.getBytes(charset));
			outputStream.write(endByte);
		}

		outputStream.write(endByte);
		outputStream.write(endByte);

		outputStream.flush();
	}
}
