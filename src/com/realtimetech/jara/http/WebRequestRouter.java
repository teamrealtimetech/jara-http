package com.realtimetech.jara.http;

import java.io.File;
import com.realtimetech.jara.http.header.type.MethodType;
import com.realtimetech.jara.http.header.type.MimeType;
import com.realtimetech.jara.http.network.message.ByteContent;
import com.realtimetech.jara.http.network.message.request.HttpRequest;
import com.realtimetech.jara.http.network.message.response.HttpResponse;
import com.realtimetech.jara.http.route.Priority;
import com.realtimetech.jara.http.route.RequestRouter;

public class WebRequestRouter extends RequestRouter {
	private File rootDirectory;

	public WebRequestRouter(Priority priority, File rootDirectory) {
		super(priority);
		this.rootDirectory = rootDirectory;
	}

	@Override
	public String requestUrl() {
		return "/*";
	}

	@Override
	public MimeType requestMimeType() {
		return MimeType.ALL;
	}

	@Override
	public MethodType requestMethod() {
		return MethodType.ALL;
	}

	private String getFileExtension(File file) {
		String name = file.getName();
		int lastIndexOf = name.lastIndexOf(".");
		
		if (lastIndexOf == -1) {
			return ""; // empty extension
		}
		
		return name.substring(lastIndexOf);
	}

	@Override
	public boolean onProcess(HttpRequest httpRequest, HttpResponse httpResponse) {
		String subUrl = getSubUrl(httpRequest);

		if (subUrl.equals("")) {
			subUrl = "index.html";
		}

		File targetFile = new File(rootDirectory.getAbsolutePath() + File.separatorChar + subUrl);

		if (targetFile.exists()) {
			ByteContent content = new ByteContent();
			content.setBytes(httpRequest.getStream().getConnection().getHttpServer().getFileStorage().getFileBytes(httpRequest, targetFile));
			content.setMimeType(MimeType.tryParseFileExtension(getFileExtension(targetFile)));
			httpResponse.setContent(content);

			httpResponse.setResponseCode(200);
		} else {
			httpResponse.setResponseCode(404);
		}

		return true;
	}
}
