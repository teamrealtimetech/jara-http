# Jara-HTTP

## 릴리즈

## 1. Jara-HTTP란?
Jara 프로젝트를 위한 자바로 구현된 HTTP2 웹 서버입니다.

## 2. 라이센스 및 남기는 말

Jara-HTTP는 [Apache License 2.0](./LICENSE.txt) 라이센스를 이용합니다, 여러분의 적극적인 이슈, 기능 피드백을 기대합니다.

```
JeongHwan, Park
+821032735003
parkjeonghwan@realtimetech.co.kr
```